var fs = require('fs');
var args = process.argv.slice(2);

args.forEach((a) => {
	var text = 	fs.readFileSync(a, {encoding : 'utf8'});
	var outtext = a.replace('.csv', '') + '.json';
	var result = [];
	var shot = { tags: [] };
	var scene = { start : 0, end : 0, shots : [] };
	var lines = text.split('\n').map((l) => l.trim());
	
	for (i in lines) {
		var p = lines[i].split(',');
		var val = p[0];
		var time = parseFloat(p[1]);
		if (val == '[scene]') {
			scene.end = time;
			shot.end = time;
			if (shot.end > shot.start) { scene.shots.push(shot); } 
			if (scene.end > scene.start) { result.push(scene); }
			shot = { tags: [], start: time, end: null };
			scene = { start : time, end : 0, shots : [] };
		} else if (val == '[end]') {
			scene.end = time;
			shot.end = time;
			if (shot.end > shot.start) { scene.shots.push(shot); } 
			if (scene.end > scene.start) { result.push(scene); }
		} else if (val == '[shot]') {
			shot.end = time;
			if (shot.end > shot.start) { scene.shots.push(shot); } 
			shot = { tags: [], start: time, end: null };
		} else {
			if (val)
				shot.tags.push({ name: val, time: time });
		}
	}
	var outp = JSON.stringify({ fps: 29.97, scenes: result }, null, '\t');
	fs.writeFileSync(outtext, outp);	
});

