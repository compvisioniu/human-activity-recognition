var swig = require('swig');
var fs = require('fs');

var files = process.argv.slice(2);

files.forEach((f) => {
	var text = fs.readFileSync(f, 'utf8');
	var gt = JSON.parse(text);
	gt.countForShot = function (sh) { return sh.tags.length; };
	gt.countForScene = function (s) { var sum = 0; for (i in s.shots) sum += this.countForShot(s.shots[i]); return sum; };
	var temlpt = swig.compileFile('report.swig');
	var html = temlpt( { o : gt });
	fs.writeFileSync(f.replace('.json', '') + '.html', html);
});

