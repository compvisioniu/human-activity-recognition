function result = localMax(data, wsize)
%Function build matrix of local maximums in floating window wsize*wsize
    dsize = size(data);
    mdata = zeros(dsize(1)+(wsize-mod(dsize(1),wsize)),dsize(2)+(wsize-mod(dsize(2),wsize)));
    mdata(1:dsize(1),1:dsize(2)) = data;
    msize = size(mdata);
    result = zeros(msize(1)/wsize,msize(2)/wsize);
    for i = 1 : msize(1)/wsize
        for j = 1 : msize(2)/wsize
            result(i,j) = max(max(mdata(((i-1)*wsize+1 : i*wsize),((j-1)*wsize+1 : j*wsize))));
        end
    end
end

