function Nedge = getNedge(E1,E2,E3,t)
    if E1 > t || E2 > t || E3 > t
        Nedge = 1;
    else
        Nedge = 0;
    end;
end

