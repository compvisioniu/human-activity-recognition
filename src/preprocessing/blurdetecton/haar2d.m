function [O,V,D,H] = haar2D(data, pyramidLevel)
    V = cell(pyramidLevel);
    D = cell(pyramidLevel);
    H = cell(pyramidLevel);
    O = gpuArray(data);
    for i = 1 : pyramidLevel
        [O V{i}] = haar1d(O);
        [O H{i}] = haar1d(O');
        [V{i} D{i}] = haar1d(V{i}');
        O = O';
        V{i} = V{i}';
        D{i} = D{i}';
        H{i} = H{i}';
    end;
    O = gather(O);
    for i = 1 : pyramidLevel
        V{i} = gather(V{i});
        D{i} = gather(D{i});
        H{i} = gather(H{i});
    end;
end

