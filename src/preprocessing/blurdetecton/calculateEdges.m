function [blurFlag, blurExtend] = CalculateEdges(Em, threshold, MinZero)
% Function impliment algorhytm 2 from the paper: http://tonghanghang.org/pdfs/icme04_blur.pdf.
% Returns:
% blurFlag - is 1, if image is blurred. is 0, if image is un-blurred;
% blurRate - the amount of blur in scale from 0 (unblurred) to 1

    gEm = cell(3);
    gEm{1} = gpuArray(Em{1});
    gEm{2} = gpuArray(Em{2});
    gEm{3} = gpuArray(Em{3});
    
    Nedge = arrayfun(@getNedge,gEm{1},gEm{2},gEm{3},threshold); %step 2
    Nda = arrayfun(@getNda,gEm{1},gEm{2},gEm{3}); %step 3
    Nrg = arrayfun(@getNrg,gEm{1},gEm{2},gEm{3}); %step 4
    Nbrg = arrayfun(@getNbrg,gEm{1},threshold); %preparing for step5
    Nbrg = arrayfun(@filterNbrg,Nbrg,Nrg); %step 5
    
    Nedge = gather(sum(sum(Nedge)));
    Nda = gather(sum(sum(Nda)));
    Nrg = gather(sum(sum(Nrg)));
    Nbrg = gather(sum(sum(Nbrg)));
    
    Per = Nedge/Nda; %step 6
    blurExtend = Nbrg/Nrg; %step 7
    
    if Per > MinZero
        blurFlag = 0;
    else
        blurFlag = 1;
    end;
end

