function [blurFlag, blurRate] = detectBlur(image)
% Function is used for detect any kind of blur on image.
% Returns:
% blurFlag - is 1, if image is blurred. is 0, if image is un-blurred;
% blurRate - blur confident coefficient 
% function base on paper: http://tonghanghang.org/pdfs/icme04_blur.pdf
threshold = 35; 
minzero = 0.07;
[O,LH,HH,HL] = haar2d(image,3); % Apply Haar transform for image, algorhytm 1 (step 1) from the paper
E = cell(3);
Em = cell(3);
wsize = [8 4 2];
E{1} = arrayfun(@geomSum,LH{1},HH{1},HL{1}); % Calcalate edge map for each scale , algorhytm 1 (step 2) from the paper
E{2} = arrayfun(@geomSum,LH{2},HH{2},HL{2});
E{3} = arrayfun(@geomSum,LH{3},HH{3},HL{3});
parfor i = 1:3
    Em{i} = localMax(E{i},wsize(i)); % calculate local maximums of edge maps, algorhytm 1 (step 3) from the paper
end;    
[blurFlag, blurRate] = calculateEdges(Em,threshold,minzero); %use algorhytm 2 from the paper
end

