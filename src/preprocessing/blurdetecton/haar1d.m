function [main,add] = haar1d(data)
    odd = double(data(1:2:end,:));
    even = double(data(2:2:end,:));
    
    oddsize = size(odd);
    oddsize = oddsize(1);
    evensize = size(even);
    evensize = evensize(1);
    
    if oddsize>evensize
       odd = odd(1:oddsize-1,:);
    end;
    if evensize>oddsize
       even = even(1:evensize-1,:);
    end;
    
    main = arrayfun(@semiSum,odd,even);
    add = arrayfun(@semiDiff,odd,even);
end
