function amount = noisedet( frame )
%NOISE Detect white noise parameters at the frame
    % filter each channel separately
    r = medfilt2(frame.fdata(:, :, 1), [7 7]);
    g = medfilt2(frame.fdata(:, :, 2), [7 7]);
    b = medfilt2(frame.fdata(:, :, 3), [7 7]);
    K = cat(3, r, g, b);                % merge back
    
    noiseM = abs(K - frame.fdata);
    
    grayM = reshape(rgb2gray(noiseM), [], 1);
    [mu, sigmahat] = normfit(grayM);
    [mur, sigmahatr] = normfit(reshape(noiseM(:,:,1), [], 1));
    [mug, sigmahatg] = normfit(reshape(noiseM(:,:,2), [], 1));
    [mub, sigmahatb] = normfit(reshape(noiseM(:,:,3), [], 1));

    amount.gray = [mu sigmahat];
    amount.r = [mur sigmahatr];
    amount.r = [mug sigmahatg];
    amount.r = [mub sigmahatb];
end

