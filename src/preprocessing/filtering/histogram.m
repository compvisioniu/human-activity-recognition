function h = histogram(frame)
%HISTOGRAM calulate 0:255 histogram for brightness 
    frame.gray = rgb2gray(frame.fdata);
    % to show histogram use
    % imhist(frame.gray)

    % count relative occurences of pixel briteness
    h = imhist(frame.gray) / numel(frame.gray);
end