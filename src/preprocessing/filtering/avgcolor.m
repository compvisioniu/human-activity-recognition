function color = avgcolor( frame )
%AVGCOLOR calculate average color of the
%   frame is a frame with all the staff

   avg = mean(mean(frame.fdata, 2), 1);     % collapse by x & y
   color = reshape(avg, 1, 3);                % reshape to single-dim arr.

end

