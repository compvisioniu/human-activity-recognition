function [localtags, data, ft, places, grouped] = cf_run_places_for_video( frames, weak_threshold, ctx)
%% recognition initalization
    display('-------------------recognition part -----------------------');
    localtags = { };
    data = [ ];
    ft = { };
        
%% recognition run
    for i = 1:length(frames)
        places(i) = cf_net_recognize(frames(i).cdata, ctx);
        [frametags, grouped(i)] = cf_places_interpret(ctx, places(i), weak_threshold);
       
        % display(frametags);
        ft{i} = frametags;
    
        frame_tag_count = length(frametags(:, 1));
        if (frame_tag_count == 0)
            continue;
        end;
        % add tags if they are not accounted yet
        for frame = 1:frame_tag_count
            tag         = frametags{frame, 1};
            probability = frametags{frame, 2};
            tagscount   = length(localtags);
            notyetdetected = 1;
            for k=1:tagscount
                if strcmp(localtags{k}, tag)
                    notyetdetected = 0;
                    break;
                end;
            end;
            if notyetdetected     % new tag found. adding to a list
                localtags(end + 1) = {tag};
            end
        end;  
    
        % writing data to result array
        for tagnumber = 1:length(localtags)
            tag    = localtags{tagnumber};
            tagidx = find(ismember(frametags(:, 1), tag));
            [tmp, tmp2] = size(tagidx);
            ispresent = tmp == 1;
            if ispresent
                data(i, tagnumber) = frametags{tagidx, 2};
            else 
                data(i, tagnumber) = 0;
            end;
        end;
        display(strcat('<<-',  num2str(i), ' processed'));
    end;
