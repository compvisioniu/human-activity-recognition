function [result, grouped ] = cf_places_interpret( ctx, data, threshold )
%CF_PLACES_INTERPRET Interprets network output and returns convolved result
   
    scores = data{1};       
    scores = scores(:,1);               % single 205x1
 
    outdoorProb = 0;
    natureProb = 0;
    cityProb = 0;
    grProb = zeros(size(ctx.grnames));
    for tgi = 1:205
        score = scores(tgi);
        if ctx.isOutdoor(tgi)
            outdoorProb = outdoorProb + score;
        end
        if ctx.isNature(tgi, 1)
            natureProb = natureProb + score;
        end
        if ctx.isNature(tgi, 2)
            cityProb = cityProb + score;
        end
        for gri = 1:size(ctx.grnames)
            if ctx.isInGroup(tgi, gri)
                grProb(gri) = grProb(gri) + score;
            end;
        end
    end
    
    grouped.outdoorProb = outdoorProb;
    grouped.natureProb = natureProb;
    grouped.cityProb = cityProb;
    grouped.grProb = grProb;
    
    scores_ordered = sort(scores);
    N = 0;
    while scores_ordered(end - N) > threshold
        N = N + 1;
    end;
    topN = scores_ordered(end-N+1:end); % top N elements
    result = cell(N, 2);
    for i=1:N
    	x = find(scores == topN(i));
        tag = strjoin( ctx.tags(1).textdata(x) );
        tag = tag(4:end);
        result(i, 1) = { tag };
        result(i, 2) = { topN(i) };
    end
end

