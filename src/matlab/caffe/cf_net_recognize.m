function result = cf_net_recognize( im, ctx )
%CF_NET_RECOGNIZE Runs image recognition on trained network
    input_data = {cf_image_normalize(im, ctx)};
    result = ctx.net.forward(input_data);
end

