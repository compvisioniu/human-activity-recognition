function [tags_outdoor, tags_grouped, tag_groups, tags_nature_or_city] = places_aggregate_tags(filename)
%PLACES_AGGREGATE_TAGS Summary of this function goes here
    file = fopen(filename, 'r');
    header = strsplit(fgetl(file), ',');
    tags_grouped = header(5:13)';
    tag_groups = zeros(205, 9);
    tags_outdoor = zeros(205);
    tags_nature_or_city = zeros(205, 2);
    for i = 1:205
        line = strsplit(fgetl(file), {','}, 'CollapseDelimiters', false);
        tags_outdoor(i) = ~strcmp(line{4}, '');
        for group = 5:13
            tag_groups(i,group - 4) = ~strcmp(line{group}, '');
        end;
        tags_nature_or_city(i,1) = ~strcmp(line{15}, '0');
        tags_nature_or_city(i,2) = ~strcmp(line{16}, '0');
    end;
end