function net = cf_net_prepare( model, weights )
%CF_NET_PREPARE Loads trained caffe CNN, e.g. plases CNN

    display('>> Starting caffe model initialization');
    tic;
    caffe.set_mode_cpu();
    % create CNN object with current weights & model 
	net = caffe.Net(model, weights, 'test'); 
    tm = num2str(toc);
    display(strcat('<< Caffee net loaded in  ', tm, ' sec'));
end

