---
title: "Caffe simple quide"
author: "K. Sozykin"
---

1. Installation and configuration:
--- 
    - Download preconfigured Vmvare image with linux mint
    - Unzip it and Open it in Vmware player, Login and pass is 'cvlab'
    - It's included caffe with all dependences, matlab, python.
    - if you want rebuild caffe, first change content of makefile if it nessary. Then in terminal:
        - cd $ /home/cvlab/caffe
        - $ sudo make clean
        - $ sudo make pycaffe (optional)
        - $ sudo make mataffe (optional)
        - $ sudo make test 
        - $ sudo make runtest
        - $ sudo make mattest (optional)
 
2. Working with caffe at matlab.
---

Open termenal and run matlab: ~ $ sudo /usr/local/MATLAB/R2014b/bin/matlab -desktop. 
More detail can be founded in test_caffe.m
