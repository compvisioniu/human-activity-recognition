function res = test_caffe()
% return vector of struct items with name of places category
% and it's probability
    caffe_root = '/home/cvlab/caffe/';
    model = '/home/cvlab/placesCNN/places205CNN_deploy.prototxt';
    weights = '/home/cvlab/placesCNN/places205CNN_iter_300000.caffemodel';
    caffe.set_mode_cpu();
    % create CNN object with current weights & model 
    net = caffe.Net(model, weights, 'test'); 
    im = imread('//home/cvlab/test_images/cafe.jpg');
    % resize image, and transrofm rgb --> bgr
    % see prepare image for more details
    input_data = {prepare_image(im)};
    res = net.forward(input_data);
    % get vector of probabilites of some places category
    scores = res{1};
    n = length(scores);
    i = 1;
    % locte memory for struct arrya answer
    items = repmat(struct('cat','None','prob',0.0), 0, 2 ); 
    % csv file with names of places categories 
    places = importdata('/home/cvlab/placesCNN/categoryIndex_places205.csv');
    limit_prob = 0.09
    while i < n + 1
        if scores(i) > limit_prob
            fprintf('%d\n',i);
            item = struct();
            item.cat = strjoin(places(1).textdata(i));
            item.prob = scores(i);
            items(end + 1) = item;
        end;
        i = i + 1;
    end;
    caffe.reset_all();
    res = items
    
