function ctx = cf_places_load( folder )
%CF_PLACES_LOAD load caffe places cnn from drive
    model =     fullfile(folder, 'places205CNN_deploy.prototxt');
    weights =   fullfile(folder, 'places205CNN_iter_300000.caffemodel');
    meanfile =  fullfile(folder, 'places205CNN_mean.binaryproto');
    tagfile =   fullfile(folder, 'categoryIndex_places205.csv');
    ctx.net = cf_net_prepare(model, weights);
    ctx.means = caffe.io.read_mean(meanfile);   % for image normalization
    ctx.tags = importdata(tagfile);
    [ctx.isOutdoor, ctx.grnames, ctx.isInGroup, ctx.isNature] =...
        places_aggregate_tags(fullfile(pwd, '/metadata/categories.csv'));   
end

