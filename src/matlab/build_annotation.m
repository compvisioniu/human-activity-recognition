%% builds full context for a video stream

%% settings
videofolder = '/home/cvlab/har/data/';
database =  {'workers.mp4' 'lab.mp4' 'usa.mp4' 'Riga.mp4'};
videofile = fullfile(videofolder, database{3});
fps = 1;
totalvideolength = 10; % 10000 for unlim

DO_PLACES = 1;
DO_FACES = 0;
DO_OBJECTS = 0;
DO_SHAKE = 0;

%% load video if not loaded
if ~exist('frames', 'var')
    frames = VideoToFrames(videofile, ...
                            1 / fps, ...
                            0, totalvideolength, ...
                            0.5); % resize coefficient
else
    display('<< Frames already loaded');
end;

%% 
if DO_PLACES 
    if ~exist('places_ctx', 'var')
        places_ctx = cf_places_load('/home/cvlab/placesCNN/');
    end
    build_places_tags_for_video(frames, fps, places_ctx);
end;

if DO_FACES
    build_faces_info_for_video(frames, fps);
end;

if DO_OBJECTS
    build_object_detections_for_video(frames);
end;

if DO_SHAKE
    % TODO
end;

% !python src/py/subtitles.py ../data/intervals.txt usa
% !sudo cp ../data/usa.vtt /var/www/html/data/
% !python src/py/bboxes.py ../data/bboxes.txt usa_box
% !sudo cp ../data/usa_box.json /var/www/html/data/

