function [localtags, intervals] = build_places_tags_for_video( frames, fps, places_ctx )

    weak_threshold = 0.05;
    strong_threshold = 0.2;

    try
        %% recognize
        [localtags, data, ft, places, grouped] = cf_run_places_for_video(...
                        frames, ...               % frames as a list
                        weak_threshold,  ...      % weak threshold
                        places_ctx);              % places-CNN context
                    
        display('--------------------- smoothing and displaying ----------------');
        
        
        for i=1:length(frames)
            
            display('-->> frame');
            for j = 1:length(localtags)
                if data(i,j) > 0
                    display( strcat(localtags(j), ' = ',  num2str(data(i,j))));
                end;
            end;
            grouped(i);
            display('-->> Grouped');
            for j = 1:length(places_ctx.grnames)
                if grouped(i).grProb(j) > strong_threshold
                    display(...
                        strcat( places_ctx.grnames(j), '=', ...
                                num2str(grouped(i).grProb(j))));
                end
            end
            display('<<-- End grouped');
            display('<<-- End frame');
        end
        
        dataG = data;
        %% smooth Gaussian
        win = gausswin(int32(fps / 2) * 2 + 1);
        kernel = win / sum(win);    % normalized blur
        edgesize = int32(length(kernel) / 2 - 0.5);     % integer division
        for tagnumber = 1:length(localtags)
            localtags(tagnumber) = strrep(localtags(tagnumber), '_', ' ');
            column = dataG(:, tagnumber);
            interval = conv(column, kernel);            % smoothing
            interval = interval(1+edgesize:end-edgesize);
            dataG(:, tagnumber) = interval;
        end;
        % threshold
        dataG(dataG < strong_threshold) = 0;
        data = dataG;
        
        %% smooth HMM
        % todo
        
        
        %% displaying 
        if 0
            figure
            plot(data)
            xlabel(strcat('frames (n), FPS=', num2str(fps)))
            ylabel('tag probability')
            legend(localtags)
        end

    %% clean up and aggregate
        for tagid = 1:length(localtags)           % consider each tag
            N = length(data(:, tagid));         % take the data for this tag
            frame = 1;                          
            interval = 0;
            intervals(tagid, 1, 1) = 0;             % init line
            if max(data(:, tagid)) == 0 
                continue;
            end;

            while frame <= N
                while frame <= N
                    if data(frame, tagid) > 0       % search for first non-zero value
                        break;
                    end;
                    frame = frame + 1;
                end;
                if (frame <= N)                     % either reached end of video, or non-0
                    start = frame;
                    while frame <= N
                        if data(frame, tagid) == 0  % exit if again 0
                            break;
                        end;
                        frame = frame + 1;    
                    end;
                    fin = frame;
                    if ((fin - start) / fps > 0.5)  % frame is shorter than 0.5s
                        interval = interval + 1;
                        intervals(tagid, interval, 1) = start / fps;
                        intervals(tagid, interval, 2) = fin / fps;
                    end;
                end;
            end;
        end;                        
                    
        [fintervals,err] = fopen('../data/intervals.txt', 'w');
        
        % write intervals to file
        intervalsize = size(intervals);
        for i=1:intervalsize(1)
            fprintf(fintervals, '%s', localtags{i});
            for q=1:length(intervalsize(2))
                if (intervals(i, q, 2) == 0) 
                    break;
                end;
                fprintf(fintervals, '\t%f %f', ...
                        intervals(i, q, 1),...
                        intervals(i, q, 2));
            end;
            fprintf(fintervals, '\r\n');
        end;
        fclose(fintervals);
    catch err
        err
        display('<< failed to run CNN');
    end;
end