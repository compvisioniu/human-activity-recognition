clc;                                % Clear the command window.
close all;                          % Close all figures (except those of imtool.)
imtool close all;                   % Close all imtool figures.
clear;                              % Erase all existing variables.
workspace;                          % Make sure the workspace panel is showing.

%% variables
videosrc = 'E:\data\lab.mp4';       % video source identifier
interval = 3;                       % 1 sec between frames

%% load frames from video source
if strncmpi(videosrc, 'http://www.youtube.com', 22)
    disp(['Processing youtube video: ' videosrc]);
    frames = YouTubeToFrames(videosrc, interval);       % youtube
elseif strncmpi(videosrc, 'http', 4)
    disp(['Processing camera video: ' videosrc]);
    frames = CameraToFrames(videosrc, 0, interval, 5);  % camera
else 
    disp(['Processing videofile: ', videosrc]);
    frames = VideoToFrames(videosrc, interval, 0);      % file
end

%% calculate image parameters
for i = 1:length(frames)
    frames(i).context.hist = histogram(frames(i));
    frames(i).context.avg = avgcolor(frames(i));
    frames(i).context.noise = noisedet(frames(i));
end

%% display results
% extract all contexts as a matrix
contexts = cell2mat(extractfield(frames, 'context'));
% get avg color mesh
avgs = reshape(cell2mat(num2cell(extractfield(contexts, 'avg'))), [], 3);

% get noise
noises = cell2mat(extractfield(contexts, 'noise'));
noises = [noises.gray];
noises = noises(1:2:end);

%% plot average color of frames
hold on;
xlabel('R'); ylabel('G'); zlabel('B');
plot3(avgs(:, 1), avgs(:, 2), avgs(:,3), '*');
plot3(128, 128, 128, 'x', 'color', 'red');
grid on;
axis([0 255 0 255 0 255]);
hold off;

%% plot avg noise amplitude
plot(noises)
