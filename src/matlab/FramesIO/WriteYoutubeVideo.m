function WriteYoutubeVideo(link,attempt)
%Function tried to download video from link 3 times with interval 3 sec.
%   Use it like WriteYoutubeVideo(link,0).
%   Video saved to file yvideo.mp4.
if(attempt==3)
    return;
end
try
    urlwrite(link,'yvideo.mp4');
catch
    warning('Problem using url. Trying to reconnect.');
    pause(3);
    WriteYoutubeVideo(link, attempt+1)
end
end

