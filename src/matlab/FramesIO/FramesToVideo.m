function result = FramesToVideo(outputFile,frames, codec )
%Fuction write a video, which consist of frames
%   outputFile - name of video file, which will created
%   frames - struct, which consist images data in field cdata
%   codec - codec for writing video (e.g. 'MPEG-4', 'Uncompressed AVI'...)

if(~isfield(frames,'cdata'))
   disp('Incorrect frames structure');
   result = 0;
   return;
end

%Create video out objecty
videoOutput = VideoWriter(outputFile,codec);

%WRITING

open(videoOutput);
for i = 1 : size(frames)
    writeVideo(videoOutput, frames(i).cdata);
end
close(videoOutput);

result = 1;
end

