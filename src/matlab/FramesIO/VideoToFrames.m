function frames = VideoToFrames(inputFile, interval, timeFrom, timeTo, resize)
% Function convert video from file to sequence of frames and return it.
%   VideoToFrames(inputFile, interval,timeFrom, timeTo)
%   inputFile - path to video file (of just file name, if it in same folder)
%   interval - time between readed frames in seconds, if 0 then each frame
%   timeFrom - time in seconds after that video will converted
%   timeTo - time in seconds before that video will converted
%       of video will be converted

%PREPARING
    % create video object.
    videoInput = VideoReader(inputFile);
    if ~exist('resize', 'var')
        resize = 1;
    end;
    
    % create struct for frame sequence in RGB. 
    % For access to image use frames(number).cdata.
    heigth = videoInput.get('Height') * resize;
    width = videoInput.get('Width') * resize;
    frames = struct('cdata', zeros(heigth, width, 3, 'uint8'), ...
                    'colormap', []);

    % If interval var isn`t defined, define it as 0 for standart frame rate.
    if ~exist('interval', 'var')
        interval = 1 / videoInput.get('FrameRate');
    end
        
    % If time interval isn`t defined, define it as full video time. 
    if ~exist('timeFrom', 'var') 
        timeFrom = 0;
    end
    if ~exist('timeTo', 'var')
        timeTo = videoInput.get('Duration');
    end
    
    % CHECKING
    if(timeTo > videoInput.get('Duration'))
        timeTo = videoInput.get('Duration');
        warning('Left boundary of time interval is more then duration. It is redefined.');
    end;

    disp(['Reading from ', num2str(timeFrom), ' to ', num2str(timeTo), ...
          ' each ', num2str(interval)]);
    % READING
    k = 1;
    ct = 0;
    videoInput.CurrentTime = timeFrom;
    while ct < timeTo
        videoInput.CurrentTime = ct;
        if hasFrame(videoInput)
            frames(k).cdata = imresize(readFrame(videoInput), resize);
        else
            break;
        end;
      
        disp([num2str(k), ' frames loaded at ', ...
              num2str(videoInput.CurrentTime)]);
        ct = ct + interval;
        k = k + 1;
    end
end