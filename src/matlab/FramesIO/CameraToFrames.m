function frames = CameraToFrames(url, count, interval, timeout)
%Function get image from IP camera (MATLAB IP camera support package
%required), and put it into frames sequance
%   url - camera url (e.g. http://80.24.26.85:10080/mjpg/video.mjpg)
%   count - number of frames, which will be readed
%   interval - time in seconds between read operations
%   timeout - timeout property specifies the amount of time that the snapshot function waits for data to be returned.

frames = struct('cdata',[],...
    'colormap',[]);
camera = ipcam(url,'Timeout',timeout);
for i = 1:count
	frames.cdata = snapshot(camera);
    pause(interval);
end

end

