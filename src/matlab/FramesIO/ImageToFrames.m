function frames = ImageToFrames(url, count, interval)
%Function convert image from file or remote url to frames (it is needed to get images from IP cameras in jpg)
%   url - filename (with path) or url (e.g. http://www.allaboutbaja.com/images/eastcapecam.jpg)
%   count - number of frames, which will be readed
%   interval - time in seconds between read operations


frames = struct('cdata',[],...
    'colormap',[]);
for i = 1:count
	frames.cdata = imread(url);
    pause(interval);
end

end

