function frames = YouTubeToFrames( url, interval,timeFrom, timeTo)
%Function convert video from YouTube's URL to sequence of frames and return it.
%Function get best video quality lower then 1080p.
%   YouTubeToFrames( url, interval,timeFrom, timeTo)
%   url - YouTube video URl (e.g. http://www.youtube.com/watch?v=wAfFWFZ43Uw)
%   interval - time between readed frames, if interval = 0 then each frame
%   timeFrom - time in seconds after that video will converted
%   timeTo - time in seconds before that video will converted
%       of video will be converted


%YouTubeToFrames('http://www.youtube.com/get_video_info?video_id=BAxhr0j0thY')
%Find the video ID
[s f] = regexp(url,'(\?v=)([a-zA-Z0-9\_\-]+)');
%Form the query for video info
query = strcat('http://www.youtube.com/get_video_info?video_id',url(s(1)+2:f(1)));
%Send the query
dump = webread(query);
%Decode URLs in the info dump
while ~isempty(strfind(dump,'%'))
    dump = urldecode(dump);
end

%Find the video title
[s f] = regexp(dump,'(title=)([�-��-�a-zA-Z0-9\%\-\.\=\+\[\]\s]+)');
title = dump(s:f);
%Find the area with video URLs
[s f]=regexp(dump,'\<url_encoded_fmt_stream_map.*');
url_map=dump(s:f);
%Find start and end indexes of URLs (direct links)
[s f] = regexp(url_map,'(url=)([a-zA-Z0-9\%\-\.\&\=\+\:\/\?\,\_]+)');


link_count = size(s);
link_count = link_count(2);
links = cell(2,link_count);

%For each video link
for i = 1 : link_count
    %Drop 'url='
    links{1,i}=url_map(s(i)+4 : f(i));
    %Find itag of link
    [s2 f2]=regexp(links{1,i},'(itag=)([0-9]*)');
    %Associate it with link
    links{2,i}=links{1,i}(s2+5:f2);
    %Drop attributes after ','. Get clear link.
    [s2 f2]=regexp(links{1,i},'(,[a-z]+=)');
    if(~isempty(s2))
        links{1,i}=links{1,i}(1:s2-1);
    end
    %Add title to the end of link. In other case video cant be downloaded.
    links{1,i}=strcat(links{1,i},'&',title);
end

% itag description:
% 17          3gp       176x144     
% 36          3gp       320x240     
% 5           flv       400x240     
% 43          webm      640x360     
% 18          mp4       640x360     
% 22          mp4       1280x720

%By this vector determine the itag priority of links. Itag which closer to
%vector start, has higher priority for download.
qualityVector = [22 18 43 5 36 17];
qSize = size(qualityVector);

%Find link with the hightes priority.
i=1;
while i<=qSize(2)
    for j = 1 : link_count
        if (qualityVector(i)==str2num(char(links{2,j})))
            link = links{1,j}; 
            i=link_count;
            break;
        end
    end
    i=i+1;
end

%This block drop from link dublicate of itag attribute. 
%I dont know why it happens.
[s f] = regexp(link,'(itag=)([0-9]*)');
size2=size(s);
if(size2(2)>1)
    link=strcat(link(1:s(2)-2),link(f(2)+1:length(link)));
end

%Download video to the file 'yvideo.mp4' and split it into frames.
if(exist('link','var')~=0)
    WriteYoutubeVideo(link,0);
    if(exist('yvideo.mp4','file')~=0)
        if(exist('timeFrom','var')~=0)
            frames = VideoToFrames('yvideo.mp4',interval,timeFrom,timeTo);        
        elseif(exist('interval','var')~=0)
            frames = VideoToFrames('yvideo.mp4',interval);
        else
            frames = VideoToFrames('yvideo.mp4');
        end    
    else
        warning('Data by the URL is not found');
        frames=[];
    end
else
    warning('URL is not found');
    frames=[];
end

end


