function [ heads ] = build_faces_info_for_video( frames, fps )
%build_faces_info_for_video Summary of this function goes here
%   Detailed explanation goes here

    [vheight, vwidth, vdepth] = size(frames(1).cdata);
    try
        boxes = fd_run_vj_for_video(frames);
        [fbboxes,err] = fopen('../data/bboxes.txt', 'w');
        fprintf(fbboxes, '%d, %d\r\n', vwidth, vheight);
        % write heads to file
        for frame = 1:length(boxes)
            frameinfo = boxes{frame};
            headcount = frameinfo{1};
            heads = frameinfo{2};
            fprintf(fbboxes, '%f\t%d', frame / fps, headcount);
            for pers = 1:headcount
                for point = 0:3
                    fprintf(fbboxes, '\t%f %f', ...
                        heads(pers, point * 2 + 1),...
                        heads(pers, point * 2 + 2));
                end;
            end;
            fprintf(fbboxes, '\r\n');
        end;  
        fclose(fbboxes);
    catch err
        err
        display('<< failed to run VJ: ');
    end;
end

