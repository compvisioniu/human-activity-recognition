% wnid - id of synset (image set)
% tag - description of image set
% options - objects with parametrs of GET reqest 
function [] = DownloadSynset(wnid,tag,options)
    % params for request 
    username = 'ksozykin';
    akey = 'c66a1ea6427b7dad30812235f66a0b12110c1aef';
    % create url for request
    defaultUrl = 'http://www.image-net.org/download/synset?wnid=%s&username=%s&accesskey=%s&release=latest&src=stanford'; 
    url = sprintf(defaultUrl, wnid, username, akey);
    downloadFile = websave(sprintf('%s.tar',tag),url,options);
    % unpack downloaded file
    untar(downloadFile,wnid);
    % rename folder
    movefile(wnid,tag);
end
