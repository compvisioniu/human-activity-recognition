% function of for creating hash map dictinary
% for fast renaming wnid -> data set description
function [] = GetImagNetMap
    fid = fopen('mapping.csv','r');
    % start from first line 
    line = fgetl(fid);
    % native matlab hash map
    % key is id of Synset
    % value is description of Synset
    mapping = containers.Map('KeyType','char', 'ValueType','any');
    while ischar(line)
        line = strsplit(line,';');
        key = line{1};
        val = line{2};
        mapping(key) = val;
        line = fgetl(fid);
    end
    fclose(fid);
    % save (serialize) hash map
    save('mapping.mat','mapping')
end