% function for automatic downloading 
% datasets from Image-net.org
function [] = ImageNetDownloader()
    % oppen file in append mode
    handle = fopen('ImageNetDownloader.log','at');
    % test array with id's
    wnids = {'n09266604','n09474412'};
    currentDate = datestr(datetime('now'));
	if exist('mapping.mat') == 0
		GetImagNetMap();
    end
    % load hash map
    load('mapping.mat')
	for i = 1 : length(wnids)
        % get description of Synset
        try
            tag = mapping(wnids{i});
        catch e
            fprintf(handle,'%s : %s',currentDate,e.message);
            continue
        end
        if exist(sprintf('%s',tag)) == 0
            currentDate = datestr(datetime('now'));
            fprintf(handle,'%s : start %s synset dowloading\n',currentDate,tag);
            options = weboptions;
            options.Timeout = 60;
            try
                DownloadSynset(wnids{i},tag,options);
            catch e
                fprintf(e.message)
                continue
            end
            fprintf(handle,'%s : finish %s synset dowloading\n',currentDate,tag);
            delete(sprintf('%s.tar',tag))
        else
            fprintf(handle,'%s : %s synset alredy exists\n',tag,currentDate);
        end
    end
    fclose(handle);
end