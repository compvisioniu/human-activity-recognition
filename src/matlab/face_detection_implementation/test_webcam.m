
vj = viola_jones;
% Create the webcam object.
cam = webcam();

% Capture one frame to get its size.
videoFrame = snapshot(cam);
frameSize = size(videoFrame);

% Create the video player object.
videoPlayer = vision.VideoPlayer('Position', [100 100 [frameSize(2), frameSize(1)]+30]);
runLoop = true;
numPts = 0;
frameCount = 0;

while runLoop && frameCount < 400

    % Get the next frame.
    videoFrame = snapshot(cam);
    videoFrameGray = rgb2gray(videoFrame);
    frameCount = frameCount + 1;
    [bbox,ovals, cnt, lb] = vj.get_bboxes(vj,videoFrameGray,3,1);
    detector = vision.CascadeObjectDetector();
    rec = step(detector,videoFrameGray);
    total = max(lb);
    cmap = hsv(total);
    if  ~isempty(bbox)
        videoFrame = insertShape(videoFrame, 'Rectangle', rec, 'LineWidth', 3,'Color','r');
        for k = 1 : total
            videoFrame = insertShape(videoFrame, 'Circle', ovals(k,1:3), 'LineWidth', 3,'Color',ovals(k,4:6));
        end
    end
    
% Display the annotated video frame using the video player object.
    step(videoPlayer, videoFrame);

    % Check whether the video player window has been closed.
    runLoop = isOpen(videoPlayer);
end

% Clean up.
clear cam;
release(videoPlayer);