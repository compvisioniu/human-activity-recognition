# -*- coding: utf-8 -*-
"""
@author: Kostya S. gogolgrind@gmail.com
"""
from PIL import Image
from os import listdir,chdir
from os.path import isfile, join
import sys

def main():
    default_path = 'C:\\Users\\Kostya\\Documents\\bitbucket\\human-activity-recognition\\face_detection_implementation\\rfaceset'
    source_dir = sys.argv[1] if len(sys.argv) > 1 else default_path
    jpg_dir = join(source_dir,"jpg")
    gif_dir = join(source_dir,"gif")
    chdir(gif_dir)
    files = [f for f in listdir(gif_dir) if  isfile(join(gif_dir,f))]
    cnt = len([f for f in listdir(jpg_dir) if  isfile(join(jpg_dir,f))])
    for fgif in files:
        cnt = cnt + 1
        fjpg = str(cnt) + ".jpg" 
        Image.open(fgif).convert('RGB').save(join(jpg_dir,fjpg))
if __name__ == "__main__":
    main()