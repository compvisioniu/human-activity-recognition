classdef viola_jones
    properties
        detector;
        eps = -1;
        min_pts = 1;
    end
    methods(Static)
        function obj = viola_jones
            obj.detector = vision.CascadeObjectDetector();
        end
        function [X0,Y0] = rotate_points(X0, Y0,xc,yc,alpha)
            X0 = (X0 - xc) * cos(alpha) + (Y0 - yc) * sin(alpha) + xc;
            Y0 = -(X0 - xc) * sin(alpha) + (Y0 - yc) * cos(alpha) + yc;
        end
        function [result_bboxes, total, lb] =  get_bboxes(obj, img, k, debug, angles)
            result_bboxes = [];
            t = imresize(img,1/k);
            center = size(t)/2;
            %angle = -50 : 5 : 50;
            if exist('angles', 'var')
                angle = angles;
            else
                angle = [0 (-45 : 5 : -5) (5 : 5 : 45)];
            end;
            bboxes = [];
            sizes = [];
            %% image's center
            x0 = center(2);
            y0 = center(1);
            for fi = angle
                h = imrotate(t,fi,'crop','bicubic');
                rec = step(obj.detector,h);
                for i = 1 : size(rec,1)
                    X1 = rec(i,1);
                    Y1 = rec(i,2);
                    X2 = X1;
                    Y2 = Y1 + rec(i,3);
                    X3 = X1 + rec(i,4);
                    Y3 = Y1;
                    X4 = X1 + rec(i,4);
                    Y4 = Y1 + rec(i,3);
                    sizes = [sizes (rec(i,4) * rec(i,4)  + rec(i,3) *  rec(i,3))];
                    XC = rec(i,4)/2 + X1;
                    YC = rec(i,3)/2 + Y1;
                    [X1, Y1] = obj.rotate_points(X1,Y1,x0,y0,degtorad(360 - fi));
                    [X2, Y2] = obj.rotate_points(X2,Y2,x0,y0,degtorad(360 - fi));
                    [X3, Y3] = obj.rotate_points(X3,Y3,x0,y0,degtorad(360 - fi));
                    [X4, Y4] = obj.rotate_points(X4,Y4,x0,y0,degtorad(360 - fi));
                    [XC, YC] = obj.rotate_points(XC,YC,x0,y0,degtorad(360 - fi));
                    rotated = [XC YC  X2 Y2 X1 Y1 X3 Y3 X4 Y4];
                    bboxes = vertcat(bboxes,k*rotated);
                end    
            end
            
            if obj.eps == -1 
               obj.eps = ceil(max(sizes) ^ 0.5);
            end
            lb = dbscan(bboxes,obj.eps,obj.min_pts);
            total = max(lb);
            result_bboxes = bboxes;
            cur_color = 1;
            %%% debug print %%%
            if debug
                cmap = hsv(total);
                figure;
                for i = 1 : size(result_bboxes,1)
                    p = result_bboxes(i,:);
                    clr = cmap(lb(i),:);
                    if (cur_color == lb(i))
                        img = insertShape(img, 'Polygon', p(3:10), 'LineWidth', 3,'Color',clr*255);
                        cur_color = cur_color + 1;
                    end
                end
                imshow(img); 
            end
            %%% ____ %%% 
        end
    end
end
