function bboxes = fd_run_vj_for_video( frames )
%FD_RUN_VJ_FOR_VIDEO Runs Viola-Jones with fixes on video
    vj = viola_jones;
    scale = 2;
    angles = [0, -25, 25];
    for frame = 1:length(frames)
        im =rgb2gray(frames(frame).cdata);
        display(strcat('VJ>> Frame #', num2str(frame), ' of-', num2str(length(frames))));
        [xboxes, cnt, colors] = vj.get_bboxes(vj, im, scale, 0, angles);
        [dx, dy] = size(xboxes);        %TODO implement 1 frame selection algorithm
        frameboxes = zeros(dx, 8);
        for b = 1:dx
            frameboxes(b,:) = xboxes(b,3:end);
        end;
        bboxes{frame} = {dx, frameboxes};
        frames(frame).bboxes = {dx, frameboxes};
    end
