# -*- coding: utf-8 -*-
"""
@author: Kostya S. gogolgrind@gmail.com
"""
from PIL import Image
from os import listdir,chdir
from os.path import isfile, join
import sys
import matplotlib.pyplot as plt

def onclick(event):
    global image_name   
    if event.xdata != None and  event.ydata != None:
        x = event.xdata
        y = event.ydata
        with open("marker.csv","a+") as m:
            m.write(str(int(x)) + " " +  str(int(y)) + ";")
        print(x,y)
    
def main():
    default_path = 'C:\\Users\\Kostya\\Documents\\bitbucket\\human-activity-recognition\\face_detection_implementation'
    source_dir = sys.argv[1] if len(sys.argv) > 1 else default_path 
    jpg_dir = join(source_dir,"rfaceset\jpg")
    files = [f for f in listdir(jpg_dir) if  isfile(join(jpg_dir,f))]
    chdir(jpg_dir)
    for f in files:
        im = plt.imread(f)
        implot = plt.imshow(im)
        with open("marker.csv","a+") as m:
            m.write(f + ";")
        cid = implot.figure.canvas.mpl_connect('button_press_event', onclick)
        plt.show()
        input('Press Enter .....')
        with open("marker.csv","a+") as m:
            m.write("\n")
if __name__ == "__main__":
    main()