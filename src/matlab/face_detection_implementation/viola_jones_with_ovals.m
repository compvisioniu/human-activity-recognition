classdef viola_jones_with_ovals
    properties
        detector;
        eps = -1;
        min_pts = 1;
    end
    methods(Static)
        function obj = viola_jones_with_ovals
            obj.detector = vision.CascadeObjectDetector();
        end
        function [X0,Y0] = rotate_points(X0, Y0,xc,yc,alpha)
            X0 = (X0 - xc) * cos(alpha) + (Y0 - yc) * sin(alpha) + xc;
            Y0 = -(X0 - xc) * sin(alpha) + (Y0 - yc) * cos(alpha) + yc;
        end
        function [result_bboxes, ovals, total, lb, marked_img] =  get_bboxes(obj,img,k,debug)
            marked_img = img;
            result_bboxes = [];
            t = imresize(img,1/k);
            center = size(t)/2;
            %angle = -50 : 5 : 50;
            angle = [0 (-45 : 5 : -5) (5 : 5 : 45)];
            sizes = [];
            %% image's center
            x0 = center(2);
            y0 = center(1);
            rotated = [];
            for fi = angle
                h = imrotate(t,fi,'crop','bicubic');
                rec = step(obj.detector,h);
                for i = 1 : size(rec,1)
                    X1 = rec(i,1);
                    Y1 = rec(i,2);
                    X2 = X1;
                    Y2 = Y1 + rec(i,3);
                    X3 = X1 + rec(i,4);
                    Y3 = Y1;
                    X4 = X1 + rec(i,4);
                    Y4 = Y1 + rec(i,3);
                    sizes = [sizes (rec(i,4) * rec(i,4)  + rec(i,3) *  rec(i,3))];
                    XC = rec(i,4)/2 + X1;
                    YC = rec(i,3)/2 + Y1;
                    [X1, Y1] = obj.rotate_points(X1,Y1,x0,y0,degtorad(360 - fi));
                    [X2, Y2] = obj.rotate_points(X2,Y2,x0,y0,degtorad(360 - fi));
                    [X3, Y3] = obj.rotate_points(X3,Y3,x0,y0,degtorad(360 - fi));
                    [X4, Y4] = obj.rotate_points(X4,Y4,x0,y0,degtorad(360 - fi));
                    [XC, YC] = obj.rotate_points(XC,YC,x0,y0,degtorad(360 - fi));
                    result_bboxes = vertcat(result_bboxes,k*[XC, YC, X2, Y2, X1, Y1, X3, Y3, X4, Y4]);
                end    
            end
            
            if obj.eps == -1 
               obj.eps = ceil(max(sizes) ^ 0.5);
            end
            lb = dbscan(result_bboxes,obj.eps,obj.min_pts);
            total = max(lb);
            ovals = [];
            color_total = [];
            % ovals is xc yc rad r g b
            for j = 1 : total
                ovals = vertcat(ovals,[0 0 0 0 0 0]);
                color_total = [color_total 0];
            end
            
            %%% debug print %%%
            if debug
                %figure;
                cmap = hsv(total);
                for i = 1 : size(result_bboxes,1)
                    p = result_bboxes(i,:);
                    clr = cmap(lb(i),:)*255;
                    ovals(lb(i),1) = ovals(lb(i),1) + p(1);
                    ovals(lb(i),2) = ovals(lb(i),2) + p(2);
                    ovals(lb(i),3) = ovals(lb(i),3) + pdist([p(3) p(4); p(9) p(10)]);
                    ovals(lb(i),4:6) = clr;
                    
                    color_total(lb(i)) = color_total(lb(i)) + 1; 
                    %img = insertShape(img, 'Polygon', p(3:10), 'LineWidth', 3,'Color',clr);
                    
                end
                %imshow(img);
                cmap = hsv(total);
                for k = 1 : total
                    clr = ovals(k,4:6);
                    ovals(k,1:3) = (ovals(k,1:3)/color_total(k));
                    ovals(k,3) = ovals(k,3)/2;
                    img = insertShape(img, 'Circle', ovals(k,1:3), 'LineWidth', 3,'Color',clr);
                end
                marked_img = img;
            end
            %%% ____ %%% 
        end
    end
end
