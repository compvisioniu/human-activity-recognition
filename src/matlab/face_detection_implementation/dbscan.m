% Density-based spatial clustering of applications with noise (DBSCAN)
% https://en.wikipedia.org/wiki/DBSCAN
% re-implemted from : http://www.mathworks.com/matlabcentral/fileexchange/52905-dbscan-clustering-algorithm

function [labels, noise] = dbscan(data,eps,min_pts)
    % disp(data);
    claster_num = 0;
    len = size(data,1);
    visit = zeros(len,1);
    labels = zeros(len,1);
    noise = zeros(len,1);
    dist = pdist2(data,data);
    for i = 1 : len
        if visit(i) == 0
            visit(i) = 1;
            neighbors = region_query(i);
            if numel(neighbors) < min_pts
                noise(i) = 1;
            else
                expand_cluster(i,neighbors);
            end
        end
    end
    function [] = expand_cluster(p, neighbors)
        k = 1;
        claster_num = claster_num + 1;
        labels(p) = claster_num;
        while k <= numel(neighbors)
            j = neighbors(k);
            if visit(j) == 0
                visit(j) = 1;
                new_neighbors = region_query(j);
                if numel(new_neighbors) >= min_pts
                    neighbors = [neighbors new_neighbors];
                end
            end
            if labels(j) == 0
                labels(j) = claster_num;
            end
            k = k + 1;
        end
    end
    function [neighbors] = region_query(i)
        neighbors = find(dist(i,:) <= eps);
    end
end
