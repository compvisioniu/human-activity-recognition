# -*- coding: utf-8 -*-
"""
@author: Kostya S. gogolgrind@gmail.com
"""
from PIL import Image
from os import listdir,chdir
from os.path import isfile, join
import sys
import numpy as np

def calc(data):
    total_true_pos = 0
    total_true_neg = 0
    total_false_pos = 0
    total_false_neg = 0
    hand = []
    vj = []
    in_circle = lambda x,y,x0,y0,r : (x0 - x)**2 + (y0 - y)**2 < r*r
    for e in data:
        if len(e) > 1:
            true_pos = 0
            true_neg = 0
            false_pos = 0
            false_neg = 0
            name = e[0][0]
            if name == "36.jpg":
                continue
            hand = e[0][1:]
            vj = e[1][1:]
            hand = [s.split(' ') for s in hand]
            vj = [s.split(' ') for s in vj]
            for point in hand:
                is_true_pos = False
                for circle in vj:
                    x = int(point[0])
                    y = int(point[1])
                    x0 = int(circle[0])
                    y0 = int(circle[1])
                    r = int(circle[2])
                    if in_circle(x,y,x0,y0,r):
                        true_pos += 1
                        is_true_pos = True
                        break
                if not is_true_pos:
                    false_pos += 1
            total_true_pos += true_pos
            total_false_pos += false_pos
            
            for circle in vj:
                is_true_pos = False
                for point in hand:
                    x = int(point[0])
                    y = int(point[1])
                    x0 = int(circle[0])
                    y0 = int(circle[1])
                    r = int(circle[2])
                    if in_circle(x,y,x0,y0,r):
                        is_true_pos = True
                        break
                if not is_true_pos:
                    false_neg += 1
            total_false_neg += false_neg
            print(name,true_pos,false_pos,false_neg)
    print("total",total_true_pos,total_false_pos,total_false_neg)

def main():
    default_path = 'C:\\Users\\Kostya\\Documents\\bitbucket\\human-activity-recognition\\face_detection_implementation\\'
    hand_csv = open(join(default_path,'hand_marker1.csv'),'r')
    vj_csv = open(join(default_path,'vj_marker1.csv'),'r')
    hand_data = [line.rstrip().split(';')[:-1] for line in hand_csv.readlines()]
    vj_data = [line.rstrip().split(';')[:-1] for line in vj_csv.readlines()]
    megred_data = [[] for _ in range(66)];
    for h in hand_data:
        idx = int(h[0][:-4])
        if len(h) > 1:
            megred_data[idx].append(h)
    for vj in vj_data:
        idx = int(vj[0][:-4])
        megred_data[idx].append(vj)
    calc(megred_data)
    
    
    
if __name__ == "__main__":
    main()