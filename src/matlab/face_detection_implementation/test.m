function [] = test()
    function [bboxes,ovals,count,cat,new_img] = test_with_crutch(img)
        %tic;
        vj = viola_jones_with_ovals;
        is_debug_mode = 1;
        [pnt,oval,cnt,cat,new_img] = vj.get_bboxes(vj,img,2, ~~is_debug_mode);
        count = cnt;
        bboxes = pnt;
        ovals = oval;
        %toc;
    end
    function [cnt,new_img] = test_witout_crutches(img)
        detector = vision.CascadeObjectDetector();
        rec = step(detector,img);
        cnt = 0;
        new_img = insertShape(img, 'Rectangle', rec, 'LineWidth', 7,'Color','black');
    end
    files = dir ('rfaceset\jpg\*.jpg');
    len = size(files);
    for i = 1 : len
        path_to_img = sprintf('rfaceset\\jpg\\%s',files(i).name);
        img = rgb2gray(imread(path_to_img));
        marked_img = img;
        [bb,ovals,count,lb,marked_img] = test_with_crutch(img);
        [count2,marked_img] = test_witout_crutches(marked_img);
        fid = fopen('vj_marker.csv','at');
        if (size(ovals,1) > 0)
            coord = ceil(ovals(:,1:3));
            k = size(coord,1);
            record = sprintf('%s',[files(i).name ';']);
            for j = 1 : k
                record = [record sprintf('%d %d %d;',coord(j,:))];
            end
            disp(record);
            %fprintf(fid,'%s\n',record);
            figure;
            imshow(marked_img);
        end
   
    end
    %fclose(fid);
end
