export GLOG_minloglevel="3"
export PYTHONPATH='/home/cvlab/python/'
export DYLD_LIBRARY_PATH='/usr/local/cuda/lib/'
export DYLD_FALLBACK_LIBRARY_PATH=/usr/local/cuda/lib:/usr/local/lib:/usr/lib:
export LD_LIBRARY_PATH=/usr/local/cuda/lib:/usr/local/lib:~/caffe/distribute/lib
