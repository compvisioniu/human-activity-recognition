from scipy import signal
from iu.ctx import Bean
from iu.framespace import Frame
import math, time
import numpy as np
import json
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN


def gaussian(data, std = 2, kersize = 5):
	kernel = signal.get_window(('gaussian', std), kersize)
	norm = reduce(lambda a, b: a+b, kernel)
	kernel = map(lambda a: a/norm, kernel)
	hker   = kersize / 2;
	lines = data[0].__len__()
	result =  [[None for x in range(lines)] for x in range(data.__len__())]
	idx_oi = range(hker, data.__len__() - hker)
	for tag in range(0, lines):
		for idx in idx_oi:
			vals = map(lambda x: data[x][tag], range(idx - hker, idx + hker + 1))
			conv = map(lambda a, b: a*b,	kernel, vals)	
			g = reduce(lambda c, d: c + d, conv, 0)
			result[idx][tag] = g
	return result


_gaussianCache = dict()


def gaussianFunction(x, sigma, mu):
	# cache integers
	if isinstance(x, int):
		v = None
		if not x in _gaussianCache:
			v = 1 / (math.sqrt(2*math.pi)*sigma)*math.e**(-0.5*(float(x-mu)/sigma)**2)
			_gaussianCache[x] = v
		else:
			v = _gaussianCache[x]
		return v
	return 1 / (math.sqrt(2*math.pi)*sigma)*math.e**(-0.5*(float(x-mu)/sigma)**2)


def bilateral(lst, sg, sf):
	idx_list = range(0, lst.__len__())
	result = list([None] * lst.__len__())
	for i in idx_list:
		x = lst[i]
		g = lambda v: gaussianFunction(v, sg, 0.0)
		f = lambda v: gaussianFunction(v, sf, 0.0)
		vals_div = lambda j, v:  f(v - x) * g(i - j)
		vals_x = lambda j, v: v * f(v - x) * g(i - j)
		
		Wp = sum(map(vals_div, idx_list, lst))
		x_ = sum(map(vals_x, idx_list, lst)) / Wp
		result[i] = x_
	return result


def bilateral_fast(lst, sg, sf):
	# print("[DH] Running fast bilateral filter for {} points with params (g={}, f={})".format(lst.__len__(), sg, sf))
	idx_list = range(0, lst.__len__())
	result = list([None] * lst.__len__())
	four_sigma = int(sg * 4)
	for i in idx_list:
		x = lst[i]
		g = lambda v: gaussianFunction(v, sg, 0.0)
		f = lambda v: gaussianFunction(v, sf, 0.0)
		vals_div = lambda j, v:  f(v - x) * g(i - j)
		vals_x = lambda j, v: v * f(v - x) * g(i - j)
		rng = range(max(0, i - four_sigma), min(i + four_sigma, idx_list[-1] + 1))
		w = 0.0
		x_ = 0.0
		for index in rng:
			w += vals_div(index, lst[index])
			x_ += vals_x(index, lst[index])
		result[i] = x_ / w
	return result

def median_filter(lst, window):
	result = list([None] * lst.__len__())
	for x in range(0, window):
		result[x] = lst[window / 2]
		result[-(x+1)] = lst[-(window / 2 + 1)]
	for x in range(0, lst.__len__() - window + 1):
		v = sorted(lst[x:x+window])
		result[x + window / 2] = v[window / 2] \
					if window%2==1 else \
					(v[window/2-1] + v[window/2]) / 2
	return result


def bilateral_by_feature(data, sg, sf):
	length = data.__len__()
	dim = data[0].__len__()
	result = [list([None] * dim) for i in range(0, length)]
	print('[FL] Bilateral filter: [{} x {}]'.format(length, dim))
	t0 = time.clock()
	for i in range(0, dim):
		series = [data[j][i] for j in range(0, length)]
		smooth = bilateral_fast(series, sg, sf)
		for j in range(0, length):
			result[j][i] = smooth[j]
	print("[FL] finished in {} sec".format(time.clock() - t0))
	return result


def median_by_feature(data, window):
	length = data.__len__()
	dim = data[0].__len__()
	result = [list([None] * dim) for i in range(0, length)]
	print('[FL] Median filter: [{} x {}]'.format(length, dim))
	t0 = time.clock()
	for i in range(0, dim):
		series = [data[j][i] for j in range(0, length)]
		smooth = median_filter(series, window)
		for j in range(0, length):
			result[j][i] = smooth[j]
	print("[FL] finished in {} sec".format(time.clock() - t0))
	return result


def variance(data):
	mean = sum(data) / data.__len__()
	var = sum(map(lambda x: (x - mean)**2, mean)) / data.__len__()
	return var


def variance_conv(matrixdata): # list[list]
	count = matrixdata.__len__()
	dim = matrixdata[0].__len__()
	mean = list([0] * dim)
	print("[DH] Calculating variances of {} clusters with {} dimensions".format(count, dim))
	for i in range(0, dim): # for each dimension
		mean[i] = sum(map(lambda v: v[i], matrixdata)) / count
	per_feature = list([0] * dim)
	for i in range(0, count):
		for j in range(0, dim):
			d = (matrixdata[i][j] - mean[j])
			per_feature[j] += d*d
	for j in range(0, dim):
		per_feature[j] /= count
	return math.sqrt(sum(per_feature))
	
	
def do_pca(samples, n_comp):
	npsamples = np.array(samples)
	pca = PCA(n_components=n_comp)
	principals = pca.fit_transform(npsamples)
	print("[DH] PCA reshape: {} -> {}".format(npsamples.shape, principals.shape))
	return principals.tolist()
	
	
def do_dbscan(samples, min_cluster_size):
	npsamples = np.array(samples)
	dbscan = DBSCAN(eps=0.08, min_samples=min_cluster_size)
	result = dbscan.fit_predict(npsamples)
	rawres = result.tolist()
	return rawres
	
	
def save_clusters(clusters, keys, fname):
	print("[DH] Saving clusters to file {}".format(fname))
	with open(fname, 'w') as f:
		f.write('#cluster\tframe_number\ttime\n')
		for cluster in sorted(clusters.keys()):
			if clusters[cluster] is None: continue
			for framenum in clusters[cluster]:
				f.write('{}\t{}\t{}\n'.format(cluster, framenum, keys[framenum]))


def load_clusters_time(fname):
	print("[DH] Loading clusters timed from file {}".format(fname))
	lines = []
	with open(fname, 'r') as f:
		lines = map(lambda l: l.split('\t'), f.readlines())[1:]
		
	clusters = list([])
	for line in lines:
		cid = int(line[0])
		fid = int(line[1])
		time = float(line[2])
		if cid < clusters.__len__():
			clusters[cid].append(time)
		else:
			clusters.append( [ time ] )
	print("[DH] {} clusters loaded".format(len(clusters)))
	return clusters
		

def load_clusters_ids(fname):
	print("[DH] Loading clusters [ids] from file {}".format(fname))
	lines = []
	with open(fname, 'r') as f:
		lines = map(lambda l: l.split('\t'), f.readlines())[1:]
		
	clusters = list([])
	for line in lines:
		cid = int(line[0])
		fid = int(line[1])
		time = float(line[2])
		if cid < len(clusters):
			clusters[cid].append(fid)
		else:
			clusters.append( [ fid ] )
	print("[DH] {} clusters loaded".format(len(clusters)))
	return clusters
		
		
def save_gradient(grad, keys, fname):
	print("[DH] Saving gradient data to file {}".format(fname))
	with open(fname, 'w') as f:
		for i in range(0, grad.__len__()):
			f.write('{}\t{}\n'.format(keys[i], grad[i]))
			
def load_ground_truth(filename):
	with open(filename)	as f:
		data = json.load(f)
	return data
	

def closest_frame(v, arr):
	closest = 0; 
	for i in range(1, len(arr)): 
		closest = (closest if abs(v - arr[i]) >= abs(v - arr[closest]) else i)
	return closest

	
def calculate_accuracy(clusters, fps, gt, gtclusters, eps):
	realfps = gt['fps']
	borders = [x[0] for x in gtclusters]# units - time
	key = []
	# either list of dict
	if type(clusters) is list:
		keys = range(0, len(clusters))
	else:
		keys = sorted(clusters.keys())
	
	for i in keys:
		cluster = clusters[i]
		isframe = type(cluster[0]) is Frame
		
		# can be raw frame numbers or Frame objects
		if not isframe:
			start_frame = float(cluster[0])	# units - frame number in detect fps
			start_time = start_frame / fps	# units - time
		else:
			start_time = cluster[0].time
			
		if len(borders) > 0:
			cl = closest_frame(start_time, borders)
			if abs(borders[cl] - start_time) < eps:
				borders.remove(borders[cl])

	# if all borders were removed during the loop, then accuracy will be 1.0
	accuracy = 1.0 - (len(borders)  / float(len(gtclusters)))
	# oversplit - we were expecting N shots in GT, but got OS * N
	oversplit = (float(len(clusters)) + 0.0) / float(len(gtclusters))
	return accuracy, oversplit
