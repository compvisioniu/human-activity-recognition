import numpy, os, csv
import caffe, caffe.io
from caffe.proto import caffe_pb2

W = 227 	# AlexNet sample width
H = 227 	# AlexNet sample height

def setup(caffe_dir):
	caffe_dir = caffe_dir.encode('ascii','ignore')
	caffe.set_mode_cpu()
	global net, tags, blob, means
	print("Starting loading PlacesCNN")
	net = caffe.Net(os.path.join(caffe_dir, 'places205CNN_deploy.prototxt'), \
					os.path.join(caffe_dir, 'places205CNN_iter_300000.caffemodel'), caffe.TEST)
	print("PlacesCNN loaded")
	with open(os.path.join(caffe_dir, 'categoryIndex_places205.csv')) as f:
		tags = map(lambda s: "_".join(s.split(' ')[0].split('/')[2:]), f.readlines())
	blob = caffe.proto.caffe_pb2.BlobProto()
	with open(os.path.join(caffe_dir, 'places205CNN_mean.binaryproto')) as f:
		blob.ParseFromString(f.read())
		means = numpy.array( caffe.io.blobproto_to_array(blob) )


def loadtags(caffe_dir):
	caffe_dir = caffe_dir.encode('ascii','ignore')
	with open(os.path.join(caffe_dir, 'categoryIndex_places205.csv')) as f:
		tags = map(lambda s: "_".join(s.split(' ')[0].split('/')[2:]), f.readlines())
	return tags


def loadgroupings(file):
	with open(file, 'r') as f:
		heads = f.readline().strip().split(',')
		heads.remove('At all')
		result = {head: dict() for head in heads[1:]}
		f.seek(0)
		reader = csv.DictReader(f)
		for row in reader:
			for head in result.keys():
				result[head][row[heads[0]]] = (row[head] != '' and row[head] != '0')
		return result


def tag_cluster(cfg, cluster, N = 5):
	tagdir = cfg['caffe_dir'].encode('ascii','ignore')
	tags = loadtags(tagdir)
	taggings = list([0] * 205)
	for dim in range(0, 205):
		dat = map(lambda frame: frame[dim], cluster)
		taggings[dim] = sum(dat) / cluster.__len__()
	topNtags = (sorted(range(len(taggings)), key=lambda i: taggings[i])[-1:-(N+1):-1])
	R = [(id, tags[id], taggings[id]) for id in topNtags]
	
	# do groups		
	groups = loadgroupings(cfg['groupings_file'])
	group_probs = { head : 0 for head in groups.keys() }
	for head in groups.keys():
		for i in range(0, 205):
			if groups[head][tags[i]]:
				group_probs[head] += taggings[i]

	for head in group_probs.keys():
		if group_probs[head] > cfg['STRONG_THRESHOLD']:
			R.append((-1, 'G-' + head, group_probs[head]))
	
	return R


def sampling_for_places(image):
	img = numpy.array(image)
	# oversampling. 10 parts: corners, center and x-axis mirroring
	img = img[:, :, ::-1]						# change to BGR
	img = img.transpose((2, 0, 1))				# flip width and height
	data = numpy.ones((10, 3, H, W))
	data[0] = img[:, 0:H, 0:W] 								# corner
	data[1] = img[:, (image.size[1]-H):, 0:W] 				# corner
	data[2] = img[:, (image.size[1]-H):, image.size[0]-W:]	# corner
	data[3] = img[:, 0:H, image.size[0]-W:] 				# corner
	data[4] = img[:, (image.size[1]-H)/2:(image.size[1]+H)/2, (image.size[0]-W)/2:(image.size[0]+W)/2] #center
	#mirroring
	for i in range(5,10): data[i] = data[i-5][:, :, ::-1]
	#normalizing
	for i in range(0,10): 
		data[i] -= means[0, :, 0:H, 0:W]
		data[i] *= 0.5
	return data


def avg(x):
	n = x.__len__()
	return reduce(lambda c, nx: c + nx, x, 0) / n 

def median(x):
	return sorted(x.tolist())[x.__len__() / 2]

AGG_MEDIAN = 0
AGG_AVG = 1

def forward(data, aggregatemethod = AGG_MEDIAN):
	net.blobs['data'].data[...] = data
	net.forward()
	# output values are corresponding att 10 input values,
	# so we should aggregate these values for the whole image
	# either mean (avg) or median can be used for it
	out = net.blobs['prob'].data
	result = []
	if aggregatemethod == AGG_MEDIAN:
		result = [ median(out[:, x]) for x in range(0, out.shape[1]) ]
	elif aggregatemethod == AGG_AVG:
		result = [ avg(out[:, x]) for x in range(0, out.shape[1]) ]
	# return data copy
	return list(result)
