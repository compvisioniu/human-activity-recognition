import json, os

class Bean:
	def __init__(self, copy=None):
		if copy:
			self.sourcefile = copy.sourcefile
			self.datafile = copy.datafile
			self.totallength = copy.totallength
			self.data = copy.data
			self.fps = copy.fps
		else:
			self.sourcefile = ''
			self.datafile = ''
			self.totallength = -1
			self.data = []
			self.fps = 29.97

	def get_arrays(self):
		timestamps = sorted(self.data.keys())
		result = [self.data[ts] for ts in timestamps]
		return timestamps, result


	def arrays_to_dict(self, timestamps, results):
		result = dict()
		for i in range(0, timestamps.__len__()):
			result[timestamps[i]] = results[i]
		return result


	def set_data(self, timestamps, values):
		self.data = self.arrays_to_dict(timestamps, values)


	def serialize(self, filename, raw=False):
		print("[IO] Serializing {} to a file: {}".format('raw' if raw else 'bean', filename))
		timestamps, results = self.get_arrays()
		self.datafile = filename
		with open(filename, 'w') as out:
			if not raw:
				out.write('context:\n')
				out.write('   sourcefile: {}\n'.format(self.sourcefile))
				out.write('   totallength: {}\n'.format(self.totallength))
				out.write('   fps: {}\n'.format(self.fps))
				out.write('data:')
			for i in range(0, timestamps.__len__()):
				time = timestamps[i]
				vals = results[i]
				if not raw:
					out.write('\n{0:06.4f}'.format(time))
				else:
					out.write('\n')
				for val in vals:
					if raw:
						out.write('{0:06.4f}\t'.format(val))
					else:
						out.write('\t{0:06.4f}'.format(val))


	def deserialize(self, filename):
		print("[IO] Deserializing bean: {}".format(filename))
		self.datafile = filename
		with open(filename, 'r') as rf:
			lines = rf.readlines()
			i = 1
			while lines[i] != 'data:\n':
				vals = lines[i].strip().split(':')
				if vals[0] in ['totallength', 'fps']:
					setattr(self, vals[0], float(vals[1].strip()))
				else:
					setattr(self, vals[0], vals[1].strip())
				i += 1
			self.data = dict()
			for j in range(i + 1, lines.__len__()):
				vals = lines[j].split('\t')
				time = float(vals[0])
				probs = map(lambda x: float(x), vals[1:])
				self.data[time] = probs


	def length(self):
		return self.data.__len__()
		

def read_config():
	f = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../config.json')
	jsn = open(f).read()
	return json.loads(jsn)
	
class Metrix:
	def __init__(self, acc = -1, ovs = -1, purity = -1, coverage = -1, overflow = -1, ded = -1, ded5 = -1, mit = -1, mit5 = -1, mitg = -1, mitg5 = -1):
		self.accuracy = acc
		self.oversplit = ovs
		self.purity = purity
		self.coverage = coverage
		self.overflow = overflow
		self.ded = ded
		self.ded5 = ded5
		self.mit = mit
		self.mit5 = mit5
		self.mitgr = mitg
		self.mitgr5 = mitg5
	
	def __repr__(self):
		tmpl = "{0:.3f}\t{1}\t{2:.3f}\t{3:.3f}\t{4:.3f}\t{5:.3f}\t{6:.3f}\t{7:.3f}\t{8:.3f}\t{9:.3f}\t{10:.3f}"
		return tmpl.format(self.accuracy, self.oversplit, self.purity, self.coverage, self.overflow, self.ded, self.ded5, self.mit, self.mit5, self.mitgr, self.mitgr5)
