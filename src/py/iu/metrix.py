def edges_shot(shot):
	return shot[0], shot[-1]


def edges_scene(scene):
	return scene[0][0], scene[-1][-1]


def len_total(scenes):
	return scenes[-1][-1][-1] - scenes[0][0][0] + 1
	
	
def len_scene(scene):
	a, b = edges_scene(scene)
	return b - a + 1


def len_shot(shot):
	a, b = egdes_shot(shot)
	return b - a + 1


def shot_in_scene(shot, scene):
	if scene.__len__() and shot.__len__():
		scs, sce = edges_scene(scene)
		shs, she = edges_shot(shot)
		return scs <= shs and sce >= she
	else:
		return False

	
def shot_in_scene_soft(shot, scene):
	if scene.__len__() and shot.__len__():
		scs, sce = edges_scene(scene)
		shs, she = edges_shot(shot)
		return scs - shs < 3 and sce - she > 3
	else:
		return False


def scene_in_scene(scene_in, scene_out):
	if scene_in.__len__() and scene_out.__len__():
		scs, sce = edges_scene(scene_in)
		shs, she = edges_scene(scene_out)
		return scs <= shs and sce >= she
	else: 
		return False

	
def intersection(sceneWhole, sceneChecked):
	intersections = []
	for shot in sceneChecked:
		if shot_in_scene(shot, sceneWhole):
			intersections.append(shot)
	return intersections


def minus(sceneFromWhat, sceneWhat):
	result = []
	for shot in sceneFromWhat:
		if not shot_in_scene(shot, sceneWhat):
			result.append(shot)
	return result
			

def scene_overflow(scenes, gt, i):
	metr = lambda x: minus(x, gt[i]).__len__() * min(1, intersection(x, gt[i]).__len__())
	val = sum(map(metr, scenes))
	if i == 0:
		base = gt[i+1].__len__()
	elif i == len(gt) - 1:
		base = gt[i-1].__len__()
	else:
		base = gt[i-1].__len__() + gt[i+1].__len__()
	return val / (0.0 + base)


def overflow(scenes, gt):
	result = 0.0
	tc = 0.0
	for i in range(0, len(gt)):
		result += scene_overflow(scenes, gt, i) * gt[i].__len__()
		tc += gt[i].__len__()
	return result / tc

	

def scene_coverage(scenes, gtscene):
	max_scene = []
	for scene in scenes:
		isc_s = intersection(scene, gtscene)
		isc_m = intersection(max_scene, gtscene)
		max_scene = max_scene \
							if isc_m.__len__() > isc_s.__len__() \
							else scene
	return intersection(max_scene, gtscene).__len__() / (0.0 + gtscene.__len__())
	

def coverage(scenes, gt):
	result = 0.0
	tc = 0.0
	for gtscene in gt:
		result += scene_coverage(scenes, gtscene) * gtscene.__len__()
		tc += gtscene.__len__()
	return result / tc
	

def purity(clusters, gt):
	T = len_total(clusters)
	R = 1.0
	sum_outer = 0.0
	for i in range(0, len(gt)):
		x = len_scene(gt[i]) / (0.0 + T)
		s1a, s1b = edges_scene(gt[i])
		sum_inner = 0.0
		for j in range(0, len(clusters)):
			s2a, s2b = edges_scene(clusters[j])
			intersec = max(0, (min(s1b, s2b) - max(s1a, s2a) + 1))
			sum_inner += (intersec / (0.0 + len_scene(gt[i]))) ** 2
		sum_outer += x * sum_inner
	R *= sum_outer

	sum_outer = 0.0
	for j in range(0, len(clusters)):
		x = len_scene(clusters[j]) / (0.0 + T)
		s1a, s1b = edges_scene(clusters[j])
		sum_inner = 0.0
		for i in range(0, len(gt)):
			s2a, s2b = edges_scene(gt[i])
			intersec = max(0, (min(s1b, s2b) - max(s1a, s2a) + 1))
			sum_inner += (intersec / (0.0 + len_scene(clusters[j]))) ** 2
		sum_outer += x * sum_inner
	R *= sum_outer
	return R


# differential edit distance	
def ded_shots(n_result, labeled_clusters, gt, n_gt, topX = 1):
	total_shots = 0
	detected_shots = 0
	all_shots = 0
	for sc_i in range(0, len(n_gt)):
		scene = n_gt[sc_i]
		lower, upper = edges_scene(scene)
		for sh_i in range(0, len(scene)):
			shot = scene[sh_i]
			shot_tags = map(lambda s: s['name'].encode('ascii','ignore'), \
					gt['scenes'][sc_i]['shots'][sh_i]['tags'])

			match = False			
			found = False
			for sc_j in range(0, len(n_result)):
				scene_r = n_result[sc_j]
				# remove group-tags
				tags = filter(lambda x: x[0] > -1,  labeled_clusters[sc_j])
				if shot_in_scene_soft(shot, scene_r):
					found = True

					for v in range(0, min(len(tags), topX)):
						if tags[v][1] in shot_tags:
							match = True
							break
					break

			if found: total_shots += 1
			if match: detected_shots += 1
			all_shots += 1
	# print(total_shots, detected_shots, all_shots)
	return ( all_shots - detected_shots ) * 1.0 / all_shots


def get_shot_by_id(id, norm):
	sh = 0
	for scene in norm:
		for shot in scene:
			a, b = edges_shot(shot)
			if id >= a and id <= b:
				return sh
			sh += 1
	return none


def get_scene_by_id(id, norm):
	sc = 0
	for scene in norm:
		a, b = edges_scene(scene)
		if id >= a and id <= b:
			return sc
		sc += 1
	return none



def mit_metric(n_result, labeled_clusters, gt, n_gt, topX = 1):
	n = 0.0
	all = 0.0
	for sc_i in range(0, len(n_gt)):
		scene = n_gt[sc_i]
		lower, upper = edges_scene(scene)
		for sh_i in range(0, len(scene)):
			shot = scene[sh_i]
			shot_tags = map(lambda s: s['name'].encode('ascii','ignore'), \
					gt['scenes'][sc_i]['shots'][sh_i]['tags'])
			for frame in shot:
				all += 1.0
				sc = get_scene_by_id(frame, n_result)
				tags = filter(lambda x: x[0] > -1,  labeled_clusters[sc])
				match = False
				for v in range(0, min(len(tags), topX)):
					if tags[v][1] in shot_tags:
						match = True
						break
				if match: n += 1.0
	return n / all
					

def mit_metrics_groups(n_result, labeled_clusters, gt, n_gt, gdict, topX = 1):
	n = 0.0
	all = 0.0
	for sc_i in range(0, len(n_gt)):
		scene = n_gt[sc_i]
		lower, upper = edges_scene(scene)
		for sh_i in range(0, len(scene)):
			shot = scene[sh_i]
			shot_tags = map(lambda s: s['name'].encode('ascii','ignore'), \
					gt['scenes'][sc_i]['shots'][sh_i]['tags'])
			
			shot_groups = set()
			for key in gdict.keys():
				for tag in shot_tags:
					t =	tag.replace('/', '_')
					if gdict[key][t]:
						if key not in ('indoor', 'outdoor'):
							shot_groups.add(key)
		
			for frame in shot:
				all += 1.0
				sc = get_scene_by_id(frame, n_result)
				tags = map(lambda x: x[1][2:], filter(lambda x: x[0] == -1,  labeled_clusters[sc]))
				match = False
				for v in range(0, min(len(tags), topX)):
					if tags[v] in shot_groups:
						match = True
						break
				if match: n += 1.0
	return n / all

					
						
def normalize_gt(gt, frames, fps):
	scenes = []
	for scene in gt['scenes']:
		sc = list([])
		for shot in scene['shots']:
			start = shot['start'] * fps
			end = shot['end'] * fps
			sc.append(range(int(round(start)), int(round(end))))
		scenes.append(sc)
	scenes[-1][-1].append(len(frames) - 1)
	return scenes
	
	
def normalize_result(scenes, shots):
	r = list([])
	for scene in scenes:
		x = list([])
		for i in range(scene[0], scene[1] + 1):
			shot = shots[i]
			x.append(map(lambda x: x.id, shot))
		r.append(x)
	return r
