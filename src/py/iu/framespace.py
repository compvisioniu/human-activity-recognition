class Frame(object):
	def __init__(self, id, time, features):
		self.id = id
		self.time = time
		self.features = features
		self.shot = None
		self.scene = None
	
	
	def __repr__(self):
		return "{{{0}: {1:.2f} [{3}/{2}]}}".format(self.id, self.time, self.shot, self.scene)

def dist(frame1, frame2):
	metric = lambda r, b: r + b * b  # sum of squares
	grad = map(lambda a, b: a - b, frame1.features, frame2.features)
	norm = reduce(metric, grad, 0)
	return norm
	
def clust_min_dist(clust1, clust2):
	dst = float("inf")
	for frame_c1 in clust1:
		for frame_c2 in clust2:
			d = dist(frame_c1, frame_c2)
			dst = dst if dst < d else d
	return dst


def clust_max_dist(clust1, clust2):
	dist = 0
	for frame_c1 in clust1:
		for frame_c2 in clust2:
			d = dist(frame_c1, frame_c2)
			dist = dist if dist > d else d
	return dist


def get_key_frames_edge(clust):
	if len(clust) == 0: return []
	elif len(clust) == 1: return list(clust)
	else: return [ clust[0], clust[-1]]
	
	
# eps normalized 0..1. Cluster values normalized 0..1.
# max dist = dim * 1.
def get_key_frames_list(clust, eps):
	epsN = eps * len(clust)
	kf =  [clust[0]]
	last = kf[-1]
	for c in range(1, len(clust)):
		if dist(last, clust[c]) > epsN:
			kf.append(clust[c])
			last = kf[-1]
	return kf

# building chains of similarity of shots
def similarity_chains(clusters, eps, maxdist = None):
	# max dissimilarity
	epsN = eps * len(clusters[0][0].features)
	MAX_SHOT_DIST = maxdist if maxdist is not None else len(clusters) / 10

	# clusters were considered
	marked = list([-1] * len(clusters))
	mcid = 0
	# marked[0] = mcid

	# for each shot
	for shot_i in range(0, len(clusters)):
		if marked[shot_i] == -1:
			marked[shot_i] = mcid
			mcid += 1
		
		# looking forward for similar
		start = shot_i + 1
		end = min(len(clusters) - 1, shot_i + MAX_SHOT_DIST)

		if start < len(clusters):
			for shot_j in range(start, end + 1):
				dst = clust_min_dist(clusters[shot_i], clusters[shot_j])
				# if shot is close = mark it with the same chain id
				if dst < epsN:
					marked[shot_j] = marked[shot_i]
					break
				
	result = list([None] * mcid)
	for i in range(0, mcid):
		result[i] = []
		for m in range(0, len(marked)):
			if marked[m] == i:
				result[i].append(m)
	return result, marked
