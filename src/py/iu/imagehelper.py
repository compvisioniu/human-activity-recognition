import Image, cv2, math, sys, os
import caffehelper
import base64

def get_image_pyramid_from_file(file, Wlimit, Hlimit, step):
	image = Image.open(file)
	print('Creating image pyramid for "{}"'.format(file))
	# or anything more clever
	images = [image]
	d = 1
	while image.size[0] / d > 2 * Wlimit and image.size[1] / d > Hlimit:
		d *= step
		images.append(im_resize(image, d))
	return images


def im_resize(image, ratio):
	return image.resize( \
				(int(image.size[0] / ratio), int(image.size[1] / ratio)), \
				Image.ANTIALIAS)



def process_video(file, take_fps, max_time, processing_function):
	vc = cv2.VideoCapture(file)
	length = int(vc.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
	width  = int(vc.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
	height = int(vc.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
	print("Processing video '{}' with parameters\r\n\t: size={}x{}, frames={}".format(\
			file, width, height, length))
	cnt      = 0
	framecnt = 0
	samples  = []
	milestones = map(lambda x: float(x) / take_fps, range(length))
	vc.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)
	retval, image = vc.read()
	time = vc.get(cv2.cv.CV_CAP_PROP_POS_MSEC) / 1000
	escape = False
	while vc.isOpened() and cnt < len(milestones):
		while vc.isOpened() and cnt < len(milestones) and time < min(milestones[cnt], max_time):
			retval, image = vc.read()
			time = vc.get(cv2.cv.CV_CAP_PROP_POS_MSEC) / 1000
			if not retval:
				escape = True
				break
		cnt = cnt + 1

		if not vc.isOpened() or (time >= max_time) or escape:
			print("==== video end =====")
			break

		print('[{0:.2f}s] Processing frame'.format(time))
		cv2_im = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		pil_im = Image.fromarray(cv2_im)

		#TODO improve for edge cases for image ratio 2+:1
		if pil_im.size[0] > 2 * caffehelper.W and pil_im.size[1] > 2 * caffehelper.H:
			pil_im = im_resize(pil_im, 0.5 * max(pil_im.size[0], pil_im.size[1]) / caffehelper.W)
		processing_function(pil_im, time)
		
def getpreview(file, timemsec, width, imtype = 'jpg' ):
	if not os.path.isfile(file):
		return ""
	vc = cv2.VideoCapture(file)
	vc.set(cv2.cv.CV_CAP_PROP_POS_MSEC, timemsec)
	retval, image = vc.read()
	h = len(image) * width / len(image[0])
	small = cv2.resize(image, (width, h))
	txt = base64.b64encode(cv2.imencode("." + imtype, small)[1])
	return "data:image/" + imtype + ";base64," + txt

def getpreviews(file, timestamps_ms, width, imtype = 'jpg'):
	if not os.path.isfile(file):
		return ""
	vc = cv2.VideoCapture(file)
	for ts in timestamps_ms:
		vc.set(cv2.cv.CV_CAP_PROP_POS_MSEC, ts)
		retval, image = vc.read()
		h = len(image) * width / len(image[0])
		small = cv2.resize(image, (width, h))
		txt = base64.b64encode(cv2.imencode("." + imtype, small)[1])
		return "data:image/" + imtype + ";base64," + txt



