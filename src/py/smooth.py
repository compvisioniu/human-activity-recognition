import sys, os
from iu import datahelper
from iu.ctx import Bean

def smooth_median(ctx, window):
	keys, samples = ctx.get_arrays()
	smooth = datahelper.median_by_feature(samples, window)
	bean = Bean(ctx)
	bean.set_data(keys, smooth)
	bean.serialize(ctx.datafile + '.sm{}'.format(window))
	# bean.serialize(ctx.datafile + '.smr_{}'.format(window), True)
	return bean


def smooth_bilateral(ctx, sg, sf):
	keys, samples = ctx.get_arrays()
	smooth = datahelper.bilateral_by_feature(samples, sg, sf)
	bean = Bean(ctx)
	bean.set_data(keys, smooth)
	bean.serialize(ctx.datafile + '.sb{}{}'.format(int(sg), int(sf*10)))
	# bean.serialize(ctx.datafile + '.sbr_{}_{}'.format(sg, sf), True)
	return bean

if "__main__" == __name__:
	if sys.argv.__len__() < 4:
		print("parameters:")
		print("python smooth.py <filter_type> <input_file> [<window>|<sigmag> <sigmaf>]")
	else:
		filename = sys.argv[2]
		filter = sys.argv[1]
		videodata = Bean()
		videodata.deserialize(filename)
		if 'median' == filter:
			smooth_median(videodata, int(sys.argv[3]))
		elif 'bilateral' == filter:
			smooth_bilateral(videodata, float(sys.argv[3]), float(sys.argv[4]))
