import sys, os
from docutils.nodes import classifier
from iu import datahelper, caffehelper, ctx
from iu.ctx import Bean, Metrix
import iu.framespace as fsx
import numpy as np
import math
import iu.metrix as mtx

cfg = ctx.read_config()


def S(filename):
	return os.path.join(cfg['results_storage'], filename)

# maps frames to frames objects
def data_to_frames(videodata): # type: (Bean) -> list(Frame)
	tstmps, data = videodata.get_arrays()
	# converting frames to objects
	frames = list([0.0] * videodata.length())
	for i in range(0, videodata.length()):
		fr = fsx.Frame(i, tstmps[i], data[i])
		frames[i] = fr
	return frames


# build rich cluster structure (list of list of Frame)
# and update Frame shot id
def get_full_clusters(clusters, frames):
	xclusters = []
	for cl in range(0, len(clusters)):
		xclusters.append(list([]))
		for frame in clusters[cl]:
			frames[frame].shot = cl
			xclusters[cl].append(frames[frame])
	return xclusters


# make clusters smaller
def get_key_frames(clusters, videodata):
	print('[CL] Detecting key frames: trying to converge to 2-5%')
	cnt = float('inf')
	threshold = 0.0001
	step = 1.5
	keyframes_min_share = 0.03
	
	# 2-5% is a share empirically stated in s paper. We take 3%
	while cnt > keyframes_min_share * videodata.length() and threshold < 1:
		cnt = 0
		threshold = threshold * step
		for cl in range(0, len(clusters)):
			cnt += len(fsx.get_key_frames_list(clusters[cl], threshold))

	print("[CL] Converged to {0} key frames with TH={1:.2f}. Share={2:.3f}%"\
			.format(cnt, threshold, cnt * 100.0 / videodata.length()))
	clusters_reduced = list([None] * len(clusters))
	for cl in range(0, len(clusters)):
		clusters_reduced[cl] = fsx.get_key_frames_list(clusters[cl], threshold)
	return clusters_reduced


def merge_chains(rawchains, chains):
	# print("[CL] Merging chains into knots")
	knot = set()
	result = []
	for i in range(0, rawchains.__len__()):
		if len(knot) == 0:		# Knot egde found
			result.append(i)
		chain = rawchains[i]
		knot.add(chain)
		if chains.__len__() == 0:
			print("CHAINS LENGTH")
		elif chains[chain].__len__() == 0:
			print("CHAIN LENGTH: {}".format(chain))

		if chains[chain][-1] == i:
			knot.remove(rawchains[i])
	result.append(rawchains.__len__())
	finalresult = []
	for ss in range(0, result.__len__() - 1):
		finalresult.append([result[ss], result[ss+1] - 1])
	return finalresult
	

def flatten_scenes(scenes, clusters):
	scs = []
	for scene in scenes:
		sc = []
		for shot in range(scene[0], scene[1] + 1):
			sc = sc + clusters[shot]
		scs.append(sc)
	return scs

	
def acc_ovs(ctx, scenes, gt):
	gtscenes = []
	for scene in gt['scenes']:
		gtscenes.append([scene['start'], scene['end']])
	# distance allowed for an error in _actual_ frames * 3
	eps = 3.0 * ctx.fps / gt['fps'] 
	acc, ovs = datahelper.calculate_accuracy(scenes, ctx.fps, gt, gtscenes, eps)
	return acc, ovs 
	

def tag_shots(shots, videodata):
	result = []
	ts, data = videodata.get_arrays()
	for shot in shots:
		sh = map(lambda k: data[k.id], shot)
		n = caffehelper.tag_cluster(cfg, sh)
		result.append(n)
	return result
	

def tag_scenes(scenes, shots, videodata):
	result = []
	ts, data = videodata.get_arrays()
	for scene in scenes:
		X = []
		shotrg = range(scene[0], scene[1] + 1)
		shotz = map(lambda x: shots[x], shotrg) 
		for sh in shotz:
			X = X + sh
		sh = map(lambda k: data[k.id], X)
		n = caffehelper.tag_cluster(cfg, sh)
		result.append(n)
	return result


def process(eps, shots_reduced, shots, videodata, dist, gt = None, n_gt = None):
	chains, rawchains = fsx.similarity_chains(shots_reduced, eps, dist)
	scenes = merge_chains(rawchains, chains)
	tags = tag_scenes(scenes, shots, videodata)
	n_result = mtx.normalize_result(scenes, shots)			
	
	if gt is not None:
		# test
		mtxobj = Metrix()
		mtxobj.purity = mtx.purity(n_result, n_gt);
		mtxobj.coverage = mtx.coverage(n_result, n_gt)
		mtxobj.overflow = mtx.overflow(n_result, n_gt)
		mtxobj.ded 		= mtx.ded_shots(n_result, tags, gt, n_gt)
		mtxobj.ded5 	= mtx.ded_shots(n_result, tags, gt, n_gt, 5)
		mtxobj.mit 		= mtx.mit_metric(n_result, tags, gt, n_gt)
		mtxobj.mit5 	= mtx.mit_metric(n_result, tags, gt, n_gt, 5)
		
		groups = caffehelper.loadgroupings(cfg['groupings_file'])
		mtxobj.mitgr 	= mtx.mit_metrics_groups(n_result, tags, gt, n_gt, groups)
		mtxobj.mitgr5 	= mtx.mit_metrics_groups(n_result, tags, gt, n_gt, groups, 5)
				
		scenes_frames = flatten_scenes(scenes, shots)
		ACC_MIN, OVS_MAX = 0.95, 10
		acc, ovs = acc_ovs(videodata, scenes_frames, gt)
		mtxobj.accuracy = acc
		mtxobj.oversplit = ovs
		txt = ''
		# if acc > ACC_MIN and ovs < OVS_MAX:
		txt = "{0}\t{1:.5f}\t".format(dist, eps) + mtxobj.__repr__()  + '\n'
		return (n_result, tags, mtxobj, txt)
	else:
		return (n_result, tags)
		
		
def report(txt, filename_data):
	# sorted within accuracy - from worst oversplitting to best
	txt = '\n'.join(sorted(txt.split('\n'), reverse = True))
	print("==============================================")
	print("HOWFAR\tMINDIST\tACCURACY\tOVERSPLIT\tPUR\tCOV\tOVF\tDED\tDED5\tMIT\tMIT5\tMIT-G\tMIT-G5")
	print(txt)
	print("==============================================")
	fn = filename_data + '.sc.gplot'
	print('[SC] Saving scene analysis to ' + fn)
	with open(fn, 'w') as f:
	 	f.write('# max_scene_dist\tclust_dist\taccuracy\toversplitting\tpurity\tcoverage\toverflow\tded\tmit...')
	 	f.write('set xlabel \'oversplitting\'; set ylabel \'shot dist\';plot \'-\' using 2:3\n')
		f.write(txt)
		

def main(filename_data, filename_clusters, gt = None):
	videodata = Bean()
	videodata.deserialize(filename_data)
	frames = data_to_frames(videodata)

	# load shots and get key frames
	shots = get_full_clusters(datahelper.load_clusters_ids(filename_clusters), frames)
	tag_shots(shots, videodata)

	if gt is not None:
		n_gt = mtx.normalize_gt(gt, frames, videodata.fps)

	shots_reduced = get_key_frames(shots, videodata)
	if gt is not None:
		txt = ''
		rep = {}
		distprev, dist = -1, 0
		for distd in [2, 3, 4, 5, 10, 25, 50, 70, 100, 150, 200]:
			distprev = dist
			dist = max(len(shots_reduced) / distd, 1)
			if distprev == dist:
				break
			print('[GS] checking chain distance = {} shots'.format(dist))
			for ex in [1, 2, 3, 5, 7, 10]:
				eps = ex * 1e-5;
				print('\t[GS] checking for shot distance={0:.5f}'.format(eps))
				n_result, tags, mtxobj, t = \
					process(eps, shots_reduced, shots, videodata, dist, gt, n_gt)
				txt += t
				rep[(dist, eps)] = (mtxobj, n_result, tags)
		# report(txt, filename_data)
		return rep
	else:
		#TODO: parameter fine tuning
		shotdistepsilon = 0.00010
		lookforward = 17
		n_result, tags = process(shotdistepsilon, shots_reduced, shots, videodata, lookforward)
		return n_result, tags
		

if __name__ == '__main__':
	if sys.argv.__len__() < 3:
		print("Please provide params:")
		print("python group_scenes.py <bean_filename> <clusters_filename> [<ground_truth_file>]")
	else:
		if sys.argv.__len__() == 3:
			main(sys.argv[1], sys.argv[2])
		else:
			gt = datahelper.load_ground_truth(sys.argv[3])
			main(sys.argv[1], sys.argv[2], gt)
