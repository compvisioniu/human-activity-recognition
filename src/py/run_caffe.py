import sys, os, Image, uuid
from iu import caffehelper, imagehelper, datahelper, ctx
results = []
timestamps = []
context = ctx.Bean()
B = str(uuid.uuid1())


def process_image(img, time = -1):
	global results, timestamps
	data = caffehelper.sampling_for_places(img)
	outp = caffehelper.forward(data)
	results.append(outp)
	timestamps.append(time)
	with open('tmp.' + B + '.txt', 'a') as f:
		f.write('{0:.3f}'.format(time))
		for o in outp:
			f.write('\t{0:.5f}'.format(o))
		f.write('\n')


def main(filename, maxtime = 100000):
	cfg = ctx.read_config()
	print(cfg)
	caffehelper.setup(cfg["caffe_dir"])
	global results, timestamps

	if filename.endswith('jpg') or filename.endswith('png'):
		for image in imagehelper.get_image_pyramid_from_file(filename, 1.5 * caffehelper.W, 1.5 * caffehelper.H, 1.5):
			process_image(image)
	else:
		imagehelper.process_video(filename, context.fps, maxtime, process_image)
	context.set_data(timestamps, results)
	context.serialize(context.outfile)
	context.serialize(context.outfile + '.raw', True)
	

if __name__ == "__main__":
	argc = sys.argv.__len__()
	
	if argc < 3:
		print("Please provide parameters:")
		print("python run_caffe.py <source_video_file> <output_file> [<length_in_sec> [<fps>]]")
	else:
		context.sourcefile = sys.argv[1]
		context.outfile = sys.argv[2]
		context.totallength = -1 if argc < 4 else float(sys.argv[3])
		context.fps = DEFAULT_FPS if argc < 5 else int(sys.argv[4])
		if context.totallength > -1:
			main(context.sourcefile, context.totallength)
		else:
			main(context.sourcefile)
