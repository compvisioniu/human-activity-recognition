import os, sys, collections

## converts bounding boxes produces by matlab to json, that will be used to draw boxes
## in the video

## input format
rawfile = sys.argv[1];
resultname = sys.argv[2];

dir = os.path.dirname(rawfile);
inp = open(rawfile, 'r');
jsn = open(os.path.join(dir, resultname + '.json'), 'w');
lines = list(inp);
outp = '';
head = '{"size": [' + lines[0].strip() + '], "data":';

for line in lines[1:]:
	words = line.split('\t');
	tm = float(words[0]);
	cnt = 0;
	if len(words) > 2: 	# + EOL
		cnt = int(words[1]);
	outp = outp + ',\r\n{ "headcount": ' + str(cnt) + ', "time": ' + str(tm) + ', "heads": [';
	# for each head create an array [{x:v,y:v}, {x:v, y:v},...]
	for person in range(0, cnt):
		outp = outp + '[';	
		for point in range(0, 4):
			pt = words[2 + person * 4 + point].split(' ');
			x = float(pt[0]);
			y = float(pt[1]);
			outp = outp + '{ "x": ' + str(x) + ', "y": ' + str(y) + '}';
			if point < 3: outp = outp + ', ';
		outp = outp + ']';
		if person < cnt-1:  outp = outp + ', ';
	outp = outp + ']}';			

jsn.write(head + '[' + outp.strip(',') + ']}');
jsn.close();
