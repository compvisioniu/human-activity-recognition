import sys, os
from docutils.nodes import classifier
from iu import datahelper, caffehelper, ctx
from iu.ctx import Bean
import numpy as np
import iu.framespace as fsh
import math

cfg = ctx.read_config()

# prepares filename into result storage location
def S(filename):	# type: string -> string
	return os.path.join(cfg['results_storage'], filename)


# calculates absolute value of gradient for each component of feature vector
def gradient(data):  # type: list(Frames) -> list(float)
	print('[CL] Calculating gradient: {}x{}'.format(data.__len__(), data[0].features.__len__()))
	res = list([0] * (data.__len__()))
	for i in range(1, data.__len__()):
		res[i] = fsh.dist(data[i-1], data[i])
	return res

# split by peaks using NMS
def raw_clustering(grad, nsigma): # type: (list(float), float) -> list(list(int))
	classified = {0: []}
	over = []
	average = sum(grad) / len(grad)
	sigma = math.sqrt(sum(map(lambda x: (x-average)**2, grad)) / len(grad))
	threshold = average + nsigma * sigma
	print("[CL] Running threshold-based gradient clustering (avg={0:.3f}, sigma={1:.3f},thrd={2:.3f})"\
			.format(average, sigma, threshold))
	
	# find what's over threshold
	for i in range(0, grad.__len__()):
		if grad[i] > threshold:
			over.append(i)
	
	# group continuous blocks of frames
	grps = {0 : []}
	c = 0
	for i in range(0, over.__len__() - 1):
		grps[c].append(over[i])
		if (over[i+1] - over[i]) > 1:
			c+=1
			grps[c] = []
		#last element
		if i == over.__len__() - 2:
			grps[c].append(over[i+1])

	borders = [0]
	#from each group select one biggest element - NMS
	for g in sorted(grps.keys()):
		mx = grps[g][0]
		# select max gradient
		for v in grps[g][1:]:
			mx = mx if grad[v] < grad[mx] else v
		borders.append(mx)
	borders.append(len(grad))
	for i in range(0, len(borders)-1):
		classified[i] = range(borders[i], borders[i + 1])
	return classified


# creates clusters using high gradient as a class border
def gradient_clustering(ctx, Nsigma):
	print("[CL] Gradient clustering")
	keys, samples = ctx.get_arrays()
	frames = map(lambda x: fsh.Frame(x, keys[x], samples[x]), range(0, len(keys)))
	grad = gradient(frames)
	clusters = raw_clustering(grad, Nsigma)
	return clusters


# calculates accuracy for provided shots
def test(ctx, shots, ground_truth, Nsigma): # type: (Bean, list(list(int)), json, float) -> (string, float, float)
	ACCURACY_THRESHOLD = 0.7
	OVERSPLIT_THRESHOLD = 16
	txt = ''
	gtshots = []
	for scene in ground_truth['scenes']:
		for shot in scene['shots']:
			gtshots.append([shot['start'], shot['end']])
			
	# distance allowed for an error in _actual_ frames * 3
	eps = 3.0 * ctx.fps / ground_truth['fps'] 
	
	acc, ovs = datahelper.calculate_accuracy(shots, ctx.fps, ground_truth, gtshots, eps)
	s = "{}\t{}\t{}".format(acc, ovs, Nsigma)
	# if ACCURACY_THRESHOLD < acc and OVERSPLIT_THRESHOLD > ovs:
	txt += s + '\n'
	return txt, acc, ovs

# loads data
def prepare(filename, gtfilename):
	slash = filename.rfind('/')
	tail = filename[slash+1:]
	dirr = os.path.dirname(filename)
	if not os.path.isdir(os.path.join(dirr, 'oversplit')):
		os.mkdir(os.path.join(dirr, 'oversplit'))
	videodata = Bean()
	videodata.deserialize(filename)
	keys, data = videodata.get_arrays()
	hasGT = gtfilename is not None
	return keys, data, hasGT

# writes report
def report(filename, txt):
	slash = filename.rfind('/')
	tail = filename[slash+1:]
	dirr = os.path.dirname(filename)
	if not os.path.isdir(os.path.join(dirr, 'oversplit')):
		os.mkdir(os.path.join(dirr, 'oversplit'))

	report = '# accuracy\toversplit\tNsigma\tlabel\n'	
	head = "set xlabel 'accuracy'; set ylabel 'oversplit'; plot '-' using 1:2:4 with labels title '"+ tail +"'\n"

	repfile = os.path.join(dirr, 'oversplit', tail)
	print('[CL] writing oversplitting report to ' + repfile)
	fdata = open(repfile, 'w')
	fplot = open(repfile + '.gplot', 'w')
	fdata.write(report)
	fplot.write(head)
	print("==============================================")
	print("ACCURACY\tOVERSPLIT\tNSIGMA")
	print(txt)
	print("==============================================")
	fdata.write(txt)
	fplot.write(txt)
	fdata.close()
	fplot.close()


# group frames into shots
# and measure accuracy if ground thruth is provided
def main(filename, Nsigma, gtfilename = None):	# type: (string, float, string) -> (list(list), float, float)
	videodata = Bean()
	videodata.deserialize(filename)
	keys, data, hasGT = prepare(filename, gtfilename)
	if hasGT:
		gt = datahelper.load_ground_truth(gtfilename)	
	shots = gradient_clustering(videodata, Nsigma)
	datahelper.save_clusters(shots, keys, filename + '.shots')
	acc, ovs = None, None
	if hasGT:
		txt, acc, ovs = test(videodata, shots, gt, Nsigma)
		report(filename, txt)
	return shots, acc, ovs


if __name__ == '__main__':
	if sys.argv.__len__() < 3:
		print("Please provide params:")
		print("python detect_shots.py <bean_filename> <Nsigma> [<ground_truth_filename>]")
	else:
		bean_name = sys.argv[1]
		nsigma = float(sys.argv[2])
		gtfile = sys.argv[3] if sys.argv.__len__() > 3 else None
		
	main(bean_name, nsigma, gtfile)
