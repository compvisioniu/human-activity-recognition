import smooth as SMOOTH
import detect_shots as SHOTS
import group_scenes as SCENES
from iu import datahelper, metrix
from iu import imagehelper as ih
from iu.ctx import Bean
import pickle, sys
import json

tmpl = '/home/cvlab/har/results/{}/{}.txt{}'
tmpl_ovs = '/home/cvlab/har/results/{}/oversplit/{}.txt{}'
tmpl_gt = '/home/cvlab/har/repo/metadata/{}.mp4.json'


def create_json(ctx, tags, tree):
	rng = range(0, len(tree))
	arr = [None] * len(tree)
	for i in rng:
		arr[i] = dict()
		scenetags = tags[i]
		scene = tree[i]
		scene_len = metrix.len_scene(scene)
		scene_start, scene_finish = metrix.edges_scene(scene)
		scene_start = scene_start / ctx.fps
		scene_finish = scene_finish / ctx.fps
		scene_len = scene_len / ctx.fps
		arr[i]["start"] = scene_start
		arr[i]["finish"] = scene_finish
		arr[i]["length"] = scene_len
		
		base64im = ih.getpreview(ctx.sourcefile, (scene_start + scene_finish) * 500, 100)
		arr[i]["preview"] = base64im
		
		arr[i]["tags"] = dict()
		for tag in scenetags:
			arr[i]["tags"][tag[1]] = tag[2]
	return json.dumps(arr)


def save_result(ctx, tags, tree):
	rng = range(0, len(tree))
	wrtto = ctx.datafile + '.done'
	print('[RR] Writing to ' + wrtto)
	with open(wrtto, 'w') as f:
		for i in rng:
			scenetags = tags[i]
			scene = tree[i]
			scene_len = metrix.len_scene(scene)
			scene_start, scene_finish = metrix.edges_scene(scene)
			scene_start = scene_start / ctx.fps
			scene_finish = scene_finish / ctx.fps
			scene_len = scene_len / ctx.fps
			t = "Scene #{2}: {0:.2f} - {1:.2f}".format(scene_start, scene_finish, i)
			print(t)
			f.write(t + "\n")
			for tag in scenetags:
				t = "\t{0}:\t{1:.3f}".format(tag[1], tag[2])
				print(t)
				f.write(t + "\n")
	
	#TODO: restructure dumping results
	jsontext = create_json(ctx, tags, tree)
	jsonfile = ctx.datafile + '.scenes.json'
	with open(jsonfile, 'w') as f:
		f.write(jsontext)
		

def process(filename, Nsigma = 1.0, gtfilename = None):
	ctx = Bean()
	ctx.deserialize(filename)
	if (gtfilename):
		gt = None
		gt = datahelper.load_ground_truth(gtfilename)
		shots, shot_acc, shot_ovs = SHOTS.main(filename, Nsigma, gtfilename)
		fnshots = filename + '.shots'
		rep = SCENES.main(filename, fnshots, gt)
		return shot_acc, shot_ovs, rep
	else:
		shots = SHOTS.main(filename, Nsigma)
		fnshots = filename + '.shots'
		n_result, tags = SCENES.main(filename, fnshots)
		save_result(ctx, tags, n_result)

def test(videos, nsigmarange):
	result = {}
	for video in videos:
		fn = tmpl.format(video[0], video[1], video[2])
		gtfn = tmpl_gt.format(video[0])
		for nsigma in nsigmarange:
			shot_acc, shot_ovs, rep = process(fn, nsigma, gtfn)
			result[(video[0], nsigma)] = shot_acc, shot_ovs, rep
		
			with open('result.tsv', 'w') as f:
				for v in result.keys():
					vid = v[0]
					nsigma = v[1]
					shot_acc, shot_ovs, r = result[v]
					for w in r.keys():
						dist = w[0] 	# chain
						eps = w[1] 		# between shots
						mtx, n_result, tags = r[w]
						tmpx = "{0}\t{1:.2f}\t{2:.3f}\t{3:.3f}\t{4}\t{5:.5f}\t"
						s = tmpx.format(vid, nsigma, shot_acc, shot_ovs, dist, eps) + mtx.__repr__() + '\n'
						print(s.strip())
						f.write(s)


if "__main__" == __name__:
	if sys.argv.__len__() == 1:
		videos = [('usa', 10, '.sm3'), ('lgerca_lisa_1', 30, '.sm5')]
		# videos = videos[:1]
		rang = map(lambda x: x/10.0, range(-2, 11))
		test(videos, rang)
	else:
		filename = sys.argv[1]
		process(filename)
