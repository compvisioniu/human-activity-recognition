# python run_caffe.py /mnt/hgfs/data/ddt.25fps.mp4 ~/har/results/ddt.25fps.mp4.25fps.txt 100000 5
# python run_caffe.py /mnt/hgfs/data/rhcp.25fps.mp4 ~/har/results/rhcp.25fps.mp4.25fps.txt 100000 5
python run_caffe.py /mnt/hgfs/data/MPEG-7/lgerca_lisa_1.mp4 ~/har/results/lgerca_lisa_1.mp4.30fps.5.txt 100000 5
python run_caffe.py /mnt/hgfs/data/MPEG-7/lgerca_lisa_1.mp4 ~/har/results/lgerca_lisa_1.mp4.30fps.10.txt 100000 10
python run_caffe.py /mnt/hgfs/data/usa.mp4 ~/har/results/usa.mp4.30fps.5.txt 100000 5
python run_caffe.py /mnt/hgfs/data/usa.mp4 ~/har/results/usa.mp4.30fps.10.txt 100000 10
python run_caffe.py /mnt/hgfs/data/usa.mp4 ~/har/results/usa.mp4.30fps.30.txt 100000 30
python run_caffe.py /mnt/hgfs/data/MPEG-7/lgerca_lisa_1.mp4 ~/har/results/lgerca_lisa_1.mp4.30fps.30.txt 100000 30
