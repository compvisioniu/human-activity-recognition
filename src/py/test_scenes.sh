# getting shots from video
# video_id, fps, filter_set, Nsigma
if [[ $4 ]]
then
	python detect_shots.py ~/har/results/$1/$2.txt$3 $4 ~/har/repo/metadata/$1.mp4.json
	#gnuplot --persist ~/har/results/$1/oversplit/$2.txt$3.gplot
	python group_scenes.py ~/har/results/$1/$2.txt$3 ~/har/results/$1/$2.txt$3.shots ~/har/repo/metadata/$1.mp4.json
	#gnuplot --persist ~/har/results/$1/$2.txt$3.sc.gplot
else
	echo "Usage: "
	echo "	detect_shots.sh <video_name> <fps> <filters> <NSigma>"
fi
