import sys
import iu
from iu import caffehelper, datahelper, ctx
from iu.ctx import Bean

tags = []
groups = None
context = Bean()
cfg = None

def init():
	global tags, groups, cfg
	cfg = ctx.read_config()
	tags = caffehelper.loadtags(cfg['caffe_dir'])
	groups = caffehelper.loadgroupings(cfg['groupings_file'])


def build(destination):
	global context, cfg
	ts = -1
	stats = dict()
	subtitles = dict()
	with open(destination, 'w') as outpf:
		outpf.write('WEBVTT\n')
		timestamps = sorted(context.data.keys())
		for timest in timestamps:
			ts += 1
			timest = timestamps[ts]
			stats[timest] = dict()
			subtitles[timest] = dict()
			outp = context.data[timestamps[ts]]

			if outp[0] == None: continue			
			st = timest - 0.5 / context.fps
			if ts > 0: st = max(st, timestamps[ts-1] - 0.5 / context.fps) + 0.001
			en = timest + 0.5 / context.fps
			
			# video frame header
			outpf.write('\n{0}:{1:06.3f} --> {2}:{3:06.3f} align:end position:100% size:50% line:0\n'.\
							format(	int(st / 60), max(0, st - 60 * int(st / 60)), \
									int(en / 60), en - 60 * int(en / 60)))
			outpf.write('{0:06.3f}\n'.format(timest))

			for i in range(0, 205):
				stats[timest][tags[i]] = outp[i]
				if outp[i] > cfg['DETECT_THRESHOLD']:
					subtitles[timest][tags[i]] = outp[i]

			# do groups		
			group_probs = { head : 0 for head in groups.keys() }
			for head in groups.keys():
				for i in range(0, 205):
					if groups[head][tags[i]]:
						group_probs[head] += outp[i]

			for head in group_probs.keys():
				stats[timest][head] = group_probs[head]
				if group_probs[head] > cfg['STRONG_THRESHOLD']:
					subtitles[timest]['G-' + head] = group_probs[head]
		
			for key in sorted(subtitles[timest].keys()):
				outpf.write('{0}\t{1:.3f}\n'.format(key, float(subtitles[timest][key])))


def main(datafile, vttfile):
	global context
	init()
	context.deserialize(datafile)
	build(vttfile)


if __name__ == '__main__':
	datafile = sys.argv[1]
	resultfile = sys.argv[2]
	main(datafile, resultfile)

