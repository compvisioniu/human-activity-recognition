Quick start.

# Video scene estimation #
Video scene estmation is done using 2-stage process and impemented in python. At the first stage we use [AlexNet Places205-CNN](http://places.csail.mit.edu/downloadCNN.html) implemented with [caffe framework](http://caffe.berkeleyvision.org/installation.html).
Second stage is using [overlapping links](https://scholar.google.com/citations?view_op=view_citation&hl=ru&user=EoYbukgAAAAJ&citation_for_view=EoYbukgAAAAJ:d1gkVwhDpl0C).
 
Source code for video scene estimation is in the folder */src/py*.

To launch whole process of scene detection you do it in 2 steps:

* run feature extraction

```
#!bash

python run_caffe.py <video_filename.mp4> <output_featurefile.txt> [length_in_sec [fps]]
```

* (optional) In our studies we found that for fps = 10 to achieve better results we can use median filtering with windows size = 3. For fps = 30 window filtering can be 5.
```
#!bash

python smooth.py median <featurefile.txt> <window_size>
```
* run scene detection

```
#!bash

python workflow.py <featurefile.txt>
```

Result of scene detection are written to *featurefile.txt.done*